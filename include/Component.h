#ifndef Component_h
#define Component_h

#pragma once

#include <Arduino.h>
#include <PCF8574.h>
#include <Wire.h>

#include <map>
#include <vector>

#include "Configuration.h"

typedef std::function<void()> callback_t;

enum ComponentClasses { Generic,
                        Button,
                        Relay,
                        Blinds,
                        Thermometer,
                        Currentmeter };
static const std::map<String, ComponentClasses> AvailableComponentClasses = {
    {"Generic", ComponentClasses::Generic},
    {"Button", ComponentClasses::Button},
    {"Relay", ComponentClasses::Relay},
    {"Blinds", ComponentClasses::Blinds},
    {"Thermometer", ComponentClasses::Thermometer}};

enum ComponentBuses { Group,
                      Onboard,
                      I2C };
static const std::map<String, ComponentBuses> AvailableComponentBuses = {
    {"Group", ComponentBuses::Group},
    {"Onboard", ComponentBuses::Onboard},
    {"I2C", ComponentBuses::I2C}};

class component {
   protected:
    String mName;
    int16_t mID;
    uint8_t mBusAddress = 0;
    ComponentBuses mBus = ComponentBuses::Onboard;

   public:
    inline component(String name, int16_t id) : mName(name), mID(id) {}
    inline component(String name, int16_t id, uint8_t busaddress, ComponentBuses bus = ComponentBuses::Onboard) : mName(name), mID(id), mBusAddress(busaddress), mBus(bus) {}
    virtual ~component() {}

    inline void Name(String value) { mName = value; }
    inline String Name() { return mName; }
    inline void ID(int16_t value) { mID = value; }
    inline int16_t ID() { return mID; }
    inline ComponentBuses Bus() { return mBus; }
    inline uint8_t BusAddress() { return mBusAddress; }
    virtual inline const ComponentClasses Class() { return ComponentClasses::Generic; }
    virtual inline void Refresh() {}

    std::map<String, std::function<void(callback_t)>> Event;

    virtual void Control(){};
};

class components {
   private:
    std::vector<component*> mComponents;

   public:
    int16_t IndexOf(String name);
    bool Add(component* new_Component);
    int16_t Remove(int16_t index);
    int16_t Remove(String name);
    int16_t Clear();
    component* At(int16_t index);
    inline size_t Count() { return mComponents.size(); }
    inline component* operator[](int16_t index) { return At(index); }
    inline component* operator[](String name) { return At(IndexOf(name)); }

    inline std::vector<component*>::iterator begin() { return mComponents.begin(); }
    inline std::vector<component*>::iterator end() { return mComponents.end(); }

    inline void Control() {
        for (auto m : mComponents) m->Control();
    }
    inline void Refresh() {
        for (auto m : mComponents) m->Refresh();
    }
};

#endif