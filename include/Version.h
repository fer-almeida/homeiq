#ifndef Version_h
#define Version_h

#pragma once

#include <Arduino.h>

static struct version {
    const String ProductFamily = "HomeIQ";
    const String ProductName = "Pro";
    struct software {
        const uint8_t Major = 1;
        const uint8_t Minor = 0;
        const uint8_t Revision = 2;
        inline String Info() { return String(Major) + "." + String(Minor) + "." + String(Revision); }
    } Software;
    struct hardware {
        const String Model = "ESP8266 D1 Mini";
        const uint8_t Major = 1;
        const uint8_t Minor = 3;
        const uint8_t Revision = 2;
        inline String Info() { return String(Major) + "." + String(Minor) + "." + String(Revision); }
    } Hardware;
} Version;

static struct defaults {
    const uint16_t UpdateRefreshMs = 2000;
    struct components {
        struct blinds {
            const uint16_t StepMs = 250;
        } Blinds;
        struct button {
            const uint16_t DebounceMs = 50;
            const uint16_t LongClickMs = 300;
            const uint16_t DoubleClickMs = 300;
        } Button;
    } Components;
    struct restarttimer{
        const uint16_t MinInterval = 10;
        const uint16_t MaxInterval = 36000;
    } RestartTimer;
    struct files {
        const String Configuration = "/config.json";
        const String DefaultSettings = "/defaults.json";
        const String Log = "/syslog.log";
    } Files;
    struct eeprom {
        const uint16_t Size = 32;
        struct mapaddress {
            const uint16_t SerialNumber = 0;
        } MapAddress;
        struct mapsize {
            const uint16_t SerialNumber = 32;
        } MapSize;
    } EEPROM;
    struct ntp {
        const String NTPServer = "pool.ntp.org";
        const int8_t Timezone = 0;
    } NTP;
    struct i2c {
        const uint8_t Start_Address = 8;
        const uint8_t End_Address = 159;
    } I2C;
    struct telnetserver {
        const uint8_t MaxCommandParameters = 20;
        const uint8_t CommandSize = 10;
        const uint8_t HelpCommandsPerLine = 7;
        const String GuestUser = "guest";
    } TelnetServer;
} Defaults;

#endif