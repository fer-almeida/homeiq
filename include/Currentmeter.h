#ifndef Currentmeter_h
#define Currentmeter_h

#pragma once

#include <ACS712.h>
#include <Arduino.h>

#include "Component.h"
#include "Timer.h"

class device;

class currentmeter : public component {
   private:
    device *Device;
    ACS712 *mSensor;

    typedef std::function<void()> callback_t;

    float newCurrentAC = 0, newCurrentDC = 0;
    float mCurrentAC = 0, mCurrentDC = 0;

    timer *Updater;

    callback_t mCurrentACChanged;
    callback_t mCurrentDCChanged;

   public:
    currentmeter(device *device_obj, uint16_t id, String name, uint8_t busaddress, ComponentBuses bus = ComponentBuses::Onboard);
    ~currentmeter() {}

    inline const ComponentClasses Class() override { return ComponentClasses::Currentmeter; }
    inline float CurrentAC() { return mCurrentAC; }
    inline float CurrentDC() { return mCurrentDC; }

    inline bool operator==(currentmeter &rhs) { return (this == &rhs); }

    void Control() override;

    inline void OnCurrentACChanged(callback_t callback) { mCurrentACChanged = callback; }
    inline void OnCurrentDCChanged(callback_t callback) { mCurrentDCChanged = callback; }
};

#endif