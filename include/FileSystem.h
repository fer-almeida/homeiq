#ifndef FileSystem_h
#define FileSystem_h

#pragma once

#include <Arduino.h>
#include <LittleFS.h>

class filesystem {
   private:
    FSInfo info;

   public:
    filesystem();
    ~filesystem() {}

    inline size_t TotalSpace() { LittleFS.info(info); return info.totalBytes; }
    inline size_t UsedSpace() { LittleFS.info(info); return info.usedBytes; }
    inline size_t FreeSpace() { LittleFS.info(info); return info.totalBytes - info.usedBytes; }
    inline float PercentUsed() { LittleFS.info(info); return ((float)((float)info.usedBytes / (float)info.totalBytes) * 100.0); }
    inline float PercentFree() { LittleFS.info(info); return (100 - (float)((float)info.usedBytes / (float)info.totalBytes) * 100.0); }
    inline fs::File OpenFile(const String& path, const char* mode) { return LittleFS.open(path, mode); }
    inline bool DeleteFile(const String& path) { return LittleFS.remove(path); }
    bool CopyFile(const String& origin, const String& destination);
};

#endif