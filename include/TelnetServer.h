#ifndef TelnetServer_h
#define TelnetServer_h

#pragma once

#include <Arduino.h>
#include <ESPAsyncTCP.h>

#include <vector>

#include "LogTool.h"
#include "Tools.h"
#include "Version.h"

static struct telnetserver_stringtable {
    const String Unauthorized = "Unauthorized - Requires admin privileges.\r\n\r\n";
    const String MissingParameter = "Missing parameter.\r\n\r\n";
    const String InvalidParameter = "Invalid parameter.\r\n\r\n";
    const String NewNetworkConfig = "New network configuration saved. Restart device to take effect.\r\n\r\n";
} TelnetServer_StringTable;

typedef std::function<void(AsyncClient* client, String* parameter)> telnet_callback_t;

class device;

class telnetcommand {
   public:
    String Command;
    String HelpMessage;
    telnet_callback_t Callback;
    bool AdminCommand;
};

class telnetsession {
   public:
    IPAddress RemoteIP;
    uint16_t RemotePort;
    String User;
};

class telnetserver {
   private:
    telnet_callback_t mOnCommandCallback;

    uint16_t mPort;
    bool mEnabled = true;
    AsyncServer* mServer;
    String mIncomeData;
    String mLastIncomeData;
    String mCommand;
    String* mParameter = new String[Defaults.TelnetServer.MaxCommandParameters];

    device* Device;

    std::vector<telnetcommand*> mCommandList;
    std::vector<telnetsession*> mSessions;
    std::vector<String> mUsers;

    void init();

   public:
    telnetserver(device* device_obj);
    bool Reply(String message);
    uint16_t Port(uint16_t value = 0);
    inline bool Enabled() { return mEnabled; }
    void Enabled(bool value);
    String Prompt = "> ";

    inline telnetsession* CurrentSession(AsyncClient* client) { return mSessions[SessionID(client) - 1]; }
    uint8_t SessionID(AsyncClient* client);

    inline void onCommand(String command, String helpmessage, telnet_callback_t callback, bool admincommand = false) { mCommandList.push_back(new telnetcommand({command, helpmessage, callback, admincommand})); };
};

#endif