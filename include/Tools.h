#ifndef Tools_h
#define Tools_h

#pragma once

#include <Arduino.h>
#include <ESP8266WiFi.h>

#define CHECK_BIT(var, pos) ((var) & (1 << (pos - 1)))

class SString : public String {
    public:
        SString() : String() {}
        SString(uint16_t n) : String(n) {}
        SString(String str) : String(str) {}
        SString(String str, size_t sz);
        SString(const char* str, size_t sz);
        SString(const byte* str, size_t sz);

        String LimitString(size_t sz, bool fill = false);
        String LowerCase();
        String UpperCase();
        std::vector<String> Tokenize(const char separator);
};

#endif