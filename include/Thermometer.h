#ifndef Thermometer_h
#define Thermometer_h

#pragma once

#include <Arduino.h>
#include <DHT_U.h>
#include <DallasTemperature.h>
#include <OneWire.h>

#include "Component.h"
#include "Timer.h"

enum TemperatureScales { Celsius = 'C',
                         Fahrenheit = 'F' };
enum ThermometerTypes { DHT_11 = 11,
                        DHT_12 = 12,
                        DHT_21 = 21,
                        DHT_22 = 22,
                        DS_18B20 = 0 };
static const std::map<String, ThermometerTypes> AvailableThermometerTypes = {
    {"DHT11", ThermometerTypes::DHT_11},
    {"DHT12", ThermometerTypes::DHT_12},
    {"DHT21", ThermometerTypes::DHT_21},
    {"DHT22", ThermometerTypes::DHT_22},
    {"DS18B20", ThermometerTypes::DS_18B20}};

class device;

class thermometer : public component {
   private:
    device *Device;

    typedef std::function<void()> callback_t;

    ThermometerTypes mThermometerType;
    OneWire *onewire;
    DallasTemperature *dallastemperature;
    DHT_Unified *dht;
    sensors_event_t dht_event;

    timer *Updater;

    float newTemperature = 0, newHumidity = 0;
    float mTemperature = 0, mHumidity = 0;

    callback_t mTemperatureChanged, mHumidityChanged;

   public:
    thermometer(device *device_obj, uint16_t id, String name, uint8_t busaddress, ComponentBuses bus = ComponentBuses::Onboard, ThermometerTypes type = ThermometerTypes::DHT_11, TemperatureScales scale = TemperatureScales::Celsius);
    ~thermometer() {}

    inline const ComponentClasses Class() override { return ComponentClasses::Thermometer; }
    inline float Temperature() { return mTemperature; }
    inline float Humidity() { return mHumidity; }

    TemperatureScales Scale = TemperatureScales::Celsius;

    inline bool operator==(thermometer &rhs) { return (this == &rhs); }

    void Control() override;

    inline void OnTemperatureChanged(callback_t callback) { mTemperatureChanged = callback; }
    inline void OnHumidityChanged(callback_t callback) { mHumidityChanged = callback; }
};

#endif