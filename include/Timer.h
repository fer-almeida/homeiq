#ifndef Timer_h
#define Timer_h

#pragma once

#include <Arduino.h>

enum Timer_State { TimerStopped = 0,
                   TimerRunning = 1 };

class timer {
   private:
    typedef std::function<void()> callback_t;

    callback_t mOnTimeoutCallback;

    uint32_t mTimer = 0;
    uint32_t mTimeout = 0;
    Timer_State mState = Timer_State::TimerStopped;

   public:
    timer(){};
    timer(uint32_t timeout) : mTimeout(timeout){};
    ~timer(){};

    inline void Start() { if (mState == Timer_State::TimerStopped) { mState = Timer_State::TimerRunning; mTimer = millis(); }}
    inline void Reset() { if (mState == Timer_State::TimerRunning) { mState = Timer_State::TimerRunning; mTimer = millis(); }}
    inline void Stop() { mState = Timer_State::TimerStopped; mTimer = 0; }
    inline uint32_t CurrentTimerMs() { return (mState == Timer_State::TimerRunning ? millis() - mTimer : 0); }
    inline void SetTimeout(uint32_t value) { mTimeout = value; }
    inline Timer_State State() { return mState; }

    // Methods
    void Control();

    // Events
    inline void OnTimeout(callback_t callback) { mOnTimeoutCallback = callback; }
};

#endif