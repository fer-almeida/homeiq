#ifndef Device_h
#define Device_h

#pragma once

#include <Arduino.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <LittleFS.h>

#include "Blinds.h"
#include "Button.h"
#include "Component.h"
#include "Configuration.h"
#include "Currentmeter.h"
#include "DateTime.h"
#include "FileSystem.h"
#include "I2C.h"
#include "LogTool.h"
#include "MQTT.h"
#include "Network.h"
#include "Relay.h"
#include "TelnetServer.h"
#include "Thermometer.h"
#include "Timer.h"
#include "Version.h"

class device {
   private:
    timer *RestartIntervalTimer;
    timer *EpochUpdater;

    void Start_EEPROM();
    void Start_SerialPort();
    void Start_FileSystem();
    void Start_Configuration();
    void Start_LogTool();
    void Start_I2C();
    void Start_Components();
    void Start_Network();
    void Start_RestartTimer();
    void Refresh_Components();

    bool HTTPJSON_IsAuthenticated(AsyncWebServerRequest *request);
    void HTTPJSON_HandleAuthentication(AsyncWebServerRequest *request);

    bool Web_IsAuthenticated(AsyncWebServerRequest *request);
    String Web_GetSessionCookieValue(AsyncWebServerRequest *request, uint16_t index);
    void Web_HandleAuthentication(AsyncWebServerRequest *request);
    void Web_Content(String content, String mimetype, AsyncWebServerRequest *request, bool requires_authentication = true, bool static_content = false);
    void Web_ApplyUpdate(AsyncWebServerRequest *request, const String &filename, size_t index, uint8_t *data, size_t len, bool final, bool requires_authentication);
    void Web_Upload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final);

   public:
    device();
    virtual ~device() {}

    inline void Restart() { ESP.restart(); }
    inline String UpTime() { unsigned long ms = millis(); unsigned long hr = ms / 3600000; ms %= 3600000; unsigned long min = ms / 60000; ms %= 60000; unsigned long sec = ms / 1000; return String(hr) + " hours, " + min + " minutes, and " + sec + " seconds"; }

    i2c *I2C;
    HardwareSerial *SerialPort;
    network *Network;
    AsyncWebServer *WebServer;
    configuration *Configuration;
    logtool *Log;
    mqtt *MQTT;
    filesystem *FileSystem;
    telnetserver *TelnetServer;
    datetime *DateTime;

    components Components;

    String TimeStamp();

    void EEPROM_WriteInt8(uint16_t Address, int8_t Data);
    int8_t EEPROM_ReadInt8(uint16_t Address);
    void EEPROM_WriteString(uint16_t Address, String Data);
    String EEPROM_ReadString(uint16_t Address, uint16_t MaxSize = 255);

    String GetSerialNumber() { return (EEPROM_ReadInt8(Defaults.EEPROM.MapAddress.SerialNumber) == -1 ? "---NOT SET---" : EEPROM_ReadString(Defaults.EEPROM.MapAddress.SerialNumber, Defaults.EEPROM.MapSize.SerialNumber)); }
    void SetSerialNumber(String serialnumber) { EEPROM_WriteString(Defaults.EEPROM.MapAddress.SerialNumber, SString(serialnumber, Defaults.EEPROM.MapSize.SerialNumber)); }

    String Name() { return Configuration->Setting["General"]["Device Name"]; }
    void Name(String name) { Configuration->Setting["General"]["Device Name"] = name; Configuration->SaveSettings(); }
    String GetHardwareID();

    void RestartInterval(uint16_t value) { Configuration->Setting["General"]["Restart Interval"] = (value == 0 ? 0 : constrain(value, Defaults.RestartTimer.MinInterval, Defaults.RestartTimer.MaxInterval)); Configuration->SaveSettings(); }
    uint16_t RestartInterval() { return Configuration->Setting["General"]["Restart Interval"].as<uint16_t>(); }
    void RestartWhen(APMode when);
    APMode RestartWhen();
    void Control();
};

#endif