#ifndef Button_h
#define Button_h

#pragma once

#include <Arduino.h>

#include "Component.h"

enum ClickTypes { NoClick,
                  Single,
                  Double,
                  Triple,
                  Long };

class device;

class button : public component {
   private:
    device *Device;

    bool mLongClick_Detected_Retriggerable;
    bool mLongClick_Detected_Reported = false;
    bool mPressed_Triggered = false;
    bool mLongClick_Detected = false;

    uint8_t mPressedState;
    uint8_t mState;
    uint8_t mPrev_State;
    uint8_t mClick_Count = 0;

    ClickTypes mLast_Click_Type = ClickTypes::NoClick;

    PCF8574 *pcf8574;

    uint32_t mDown_Ms;
    uint32_t mDown_Time_Ms = 0;
    uint32_t mClick_Ms;
    uint32_t mDebounce_Time_Ms = Defaults.Components.Button.DebounceMs;
    uint32_t mLongClick_Detected_Counter;
    uint32_t mLongClick_Time_Ms = Defaults.Components.Button.LongClickMs;
    uint32_t mDoubleClick_Time_Ms = Defaults.Components.Button.DoubleClickMs;

    callback_t mPressedCallback, mReleasedCallback, mChangedCallback, mClickedCallback, mLongClickedCallback, mDoubleClickedCallback, mTripleClickedCallback;

   public:
    button(device *device_obj, uint16_t id, String name, uint8_t busaddress, ComponentBuses bus = ComponentBuses::Onboard);
    ~button() {}

    inline const ComponentClasses Class() override { return ComponentClasses::Button; }
    inline void DebounceTime(uint32_t ms) { mDebounce_Time_Ms = ms; }
    inline const uint32_t DebounceTime() { return mDebounce_Time_Ms; }
    inline void LongClickTime(uint32_t ms) { mLongClick_Time_Ms = ms; }
    inline const uint32_t LongClickTime() { return mLongClick_Time_Ms; }
    inline void DoubleClickTime(uint32_t ms) { mDoubleClick_Time_Ms = ms; }
    inline const uint32_t DoubleClickTime() { return mDoubleClick_Time_Ms; }
    inline void LongClickDetectedRetriggerable(bool retriggerable) { mLongClick_Detected_Retriggerable = retriggerable; }
    inline const bool LongClickDetectedRetriggerable() { return mLongClick_Detected_Retriggerable; }
    inline const bool IsPressed() { return (mState == mPressedState); }
    inline const bool IsPressedRaw() { return (digitalRead(mBusAddress) == mPressedState); }
    inline const uint32_t WasPressedFor() { return mDown_Time_Ms; }
    inline const uint8_t NumberOfClicks() { return mClick_Count; }
    inline const ClickTypes ClickType() { return mLast_Click_Type; }
    inline bool operator==(button &rhs) { return (this == &rhs); }

    inline const bool State() { return IsPressed(); }
    
    void Refresh() override;
    void Control() override;
};

#endif