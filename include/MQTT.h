#ifndef MQTT_h
#define MQTT_h

#pragma once

#include <Arduino.h>
#include <PubSubClient.h>

class device;

class mqtt {
   private:
    device *Device;
    PubSubClient *mMQTT = nullptr;

   public:
    mqtt(device *device_obj);
    ~mqtt() {}

    // Read/Write properties
    void Broker_URL(String value);
    String Broker_URL();
    void Broker_Port(uint16_t value);
    uint16_t Broker_Port();
    void User(String value);
    String User();
    void Password(String value);
    String Password();

    // Methods
    void Connect();
    void Publish(String message);
    void Publish(String subtopic, String message);
    void Control();
};

#endif