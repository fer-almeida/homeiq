#ifndef Relay_h
#define Relay_h

#pragma once

#include <Arduino.h>

#include "Component.h"

enum RelayTypes { NormallyClosed,
                  NormallyOpened };

class device;

class relay : public component {
   private:
    device *Device;

    bool mState;
    RelayTypes mRelayType;

    PCF8574 *pcf8574;

    callback_t mSettingOnCallback, mSettingOffCallback, mSetOnCallback, mSetOff, mChangedCallback;

   public:
    relay(device *device_obj, uint16_t id, String name, uint8_t busaddress, ComponentBuses bus = ComponentBuses::Onboard, RelayTypes relaytype = RelayTypes::NormallyOpened);
    ~relay() {}

    inline const ComponentClasses Class() override { return ComponentClasses::Relay; }
    inline const bool Invert() { return State(!State()); }
    inline const bool State() { return (mRelayType == RelayTypes::NormallyOpened ? !mState : mState); }
    const bool State(bool newstate, bool savestate = true);
    inline bool operator==(relay &rhs) { return (this == &rhs); }

    void Refresh() override;
    void Control() override {}
};

#endif