#ifndef Network_h
#define Network_h

#pragma once

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>

#include "Version.h"

class device;

enum APMode { Offline = 0,
              WifiClient = 1,
              SoftAP = 2,
              None = 3 };
enum ConnectionStatus { Connected = 0,
                        Disconnected = 1,
                        Unknown = 99 };
enum IPV4 { IPADDRESS = 0,
            MASK = 1,
            GATEWAY = 2 };

class network {
   private:
    int8_t mMQTT_State = -1;
    APMode mMode = APMode::Offline;
    ConnectionStatus mStatus = ConnectionStatus::Unknown;
    device *Device;

    WiFiEventHandler ConnectedEventHandler, DisconnectedEventHandler;

    inline IPAddress StringToIPAddress(String value) { return IPAddress().fromString(value); }
    bool IPV4_Set(String value, IPV4 what);

   public:
    network(device *device_obj);
    virtual ~network() {}

    // Read/Write properties
    void Hostname(String value);
    String Hostname();
    void SSID(String value);
    String SSID();
    void PSK(String value);
    String PSK();
    bool IP_Address(String value);
    void IP_Address(IPAddress value);
    IPAddress IP_Address();
    bool Gateway(String value);
    void Gateway(IPAddress value);
    IPAddress Gateway();
    bool Netmask(String value);
    void Netmask(IPAddress value);
    IPAddress Netmask();
    void DHCP(bool value);
    bool DHCP();
    void HTTP_Port(uint16_t value);
    uint16_t HTTP_Port();
    void HTTP_Enabled(bool value);
    bool HTTP_Enabled();
    void Telnet_Port(uint16_t value);
    uint16_t Telnet_Port();
    void Telnet_Enabled(bool value);
    bool Telnet_Enabled();
    void MQTT_Enabled(bool value);
    bool MQTT_Enabled();
    void NTP_Server(String value);
    String NTP_Server();
    void NTP_Timezone(int8_t tz);
    int8_t NTP_Timezone();
    void DNS_Server(uint8_t index, IPAddress value);
    void DNS_Server(uint8_t index, String value);
    IPAddress DNS_Server(uint8_t index);
    void ConnectionTimeout(uint16_t value);
    uint16_t ConnectionTimeout();

    // Read Only properties
    virtual APMode Mode() { return mMode; }
    virtual ConnectionStatus Status() { return mStatus; }
    virtual String MAC_Address() { return WiFi.macAddress(); }
    virtual bool IsConnected() { return WiFi.isConnected(); }

    // Methods
    bool Apply();
};

#endif