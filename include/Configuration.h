#ifndef Configuration_h
#define Configuration_h

#pragma once

#include <Arduino.h>
#include <LittleFS.h>
#include <ArduinoJson.h>
#include "Version.h"
#include "Tools.h"

class device;

class configuration {
    private:
        StaticJsonDocument<8192> JsonConfiguration;
        device *Device;
    public:
        configuration(device *device_obj);
        virtual ~configuration() {}

        JsonObject Setting;

        int16_t IndexOf_Component(String name);
        bool Add_Component(String cmp_class, String name, uint16_t pin, String action);
        bool Delete_Component(int16_t index);
        bool Delete_Component(String name);
        int16_t Count_Component();

        bool Authenticate(String name, String password);
        bool ChangePassword(String name, String password);
        int16_t IndexOf_User(String name);
        bool Add_User(String name, String password, bool isadmin);
        bool Delete_User(int16_t index);
        bool Delete_User(String name);
        bool Admin_User(String name);
        int16_t Count_User();

        bool SaveSettings();
        bool DefaultSettings();
};

#endif