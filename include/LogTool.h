#ifndef LogTool_h
#define LogTool_h

#pragma once

#include <Arduino.h>
#include <FS.h>
#include <LittleFS.h>
#include <WiFiUdp.h>

#include "Tools.h"

class device;

/*
LogTo

0b000000000
        |||_ Serial Port
        ||__ Syslog Server
        |___ File

LogLevel
0b000000000
       ||||_ Errors
       |||__ Warnings
       ||___ Info
       |____ Debug (Paranoic)
*/

enum LogTypes { Error,
                Warning,
                Info,
                Debug };
class logtool {
   private:
    WiFiUDP *UDPClient;
    device *Device;
    uint8_t mLogTo = 0;
    uint8_t mLogLevel = 3;

   public:
    logtool(device *device_obj);
    virtual ~logtool() {}

    void WriteToSerialPort(String Message);
    void WriteToSyslogServer(String Message);
    void WriteToFile(String Message);
    void Write(String Message, LogTypes LogType);

    inline const uint8_t LogTo() { return mLogTo; }
    inline const bool LogToNone() { return (mLogTo == 0); }
    inline const bool LogToSerialPort() { return (bool)CHECK_BIT(mLogTo, 1); }
    inline const bool LogToSyslogServer() { return (bool)CHECK_BIT(mLogTo, 2); }
    inline const bool LogToFile() { return (bool)CHECK_BIT(mLogTo, 3); }
    inline const bool LogLevelNone() { return (mLogLevel == 0); }
    inline const bool LogLevelErrors() { return (bool)CHECK_BIT(mLogLevel, 1); }
    inline const bool LogLevelWarnings() { return (bool)CHECK_BIT(mLogLevel, 2); }
    inline const bool LogLevelInfo() { return (bool)CHECK_BIT(mLogLevel, 3); }
    inline const bool LogLevelDebug() { return (bool)CHECK_BIT(mLogLevel, 4); }

    void Clear();
};

#endif