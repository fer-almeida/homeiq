#ifndef Blinds_h
#define Blinds_h

#pragma once

#include <Arduino.h>

#include "Device.h"
#include "Button.h"
#include "Relay.h"
#include "Timer.h"

enum BlindsState { Idle = 2,
                   Opening = 1,
                   Closing = 0 };

class device;

class blinds : public component {
   private:
    device *Device;

    uint8_t mCurrentPosition, mTargetPosition;
    volatile uint16_t mStepMs = Defaults.Components.Blinds.StepMs;
    BlindsState mState = BlindsState::Idle;
    volatile bool mCancel = false;

    relay *mRelayUp, *mRelayDown;
    timer *mTimerUp, *mTimerDown;

    inline String StateToString(BlindsState state) { return (state == BlindsState::Closing ? "DECREASING" : (state == BlindsState::Opening ? "INCREASING" : "STOPPED")); }

    callback_t mClosedCallback, mOpenedCallback, mBeforeCloseCallback, mBeforeOpenCallback;

   public:
    blinds(device *device_obj, uint16_t id, String name, relay* relayup, relay* relaydown);
    ~blinds() {}

    inline const ComponentClasses Class() override { return ComponentClasses::Blinds; }
    void Position(uint8_t value, bool setformerposition = false);
    inline uint8_t Position() { return mCurrentPosition; }
    inline BlindsState State() { return mState; }
    inline void Open() { mState = BlindsState::Opening; Position(100); }
    inline void Close() { mState = BlindsState::Closing; Position(0); }
    inline void StepMs(uint16_t value) { mStepMs = value; mTimerUp->SetTimeout(mStepMs); mTimerDown->SetTimeout(mStepMs); }
    inline uint16_t StepMs() { return mStepMs; }
    inline void CancelAction() { mCancel = true; }
    inline bool operator==(blinds &rhs) { return (this == &rhs); }
    
    void Refresh() override;
    void Control() override;
};

#endif