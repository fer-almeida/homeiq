#ifndef I2C_h
#define I2C_h

#pragma once

#include <Arduino.h>
#include <Wire.h>

#include <vector>

#include "Version.h"

class device;

class i2c {
   private:
    device *Device;

    uint8_t mStart_Address = Defaults.I2C.Start_Address;
    uint8_t mEnd_Address = Defaults.I2C.End_Address;

    std::vector<uint8_t> mDevices;

   public:
    i2c(device *device_obj);
    virtual ~i2c() {}

    void Enabled(bool value);
    bool Enabled();

    inline uint8_t Start_Address() { return mStart_Address; }
    inline void Start_Address(uint8_t value) { mStart_Address = value; }
    inline uint8_t End_Address() { return mEnd_Address; }
    inline void End_Address(uint8_t value) { mEnd_Address = value; }

    inline std::vector<uint8_t>::iterator begin() { return mDevices.begin(); }
    inline std::vector<uint8_t>::iterator end() { return mDevices.end(); }

    void Scan();
    inline void Clear() { mDevices.clear(); }
    inline uint8_t At(uint8_t index) { return (mDevices.size() > 0 ? mDevices[index] : 0); }
    inline uint8_t Count() { return mDevices.size(); }
    inline String Addresses() { String ret; for (auto i : mDevices) ret += (String(i) + ", "); return ret.substring(0, ret.length() - 2); }
};

#endif