#ifndef DateTime_h
#define DateTime_h

#pragma once

#include <Arduino.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#include "Timer.h"
#include "Version.h"

class datetime {
   private:
    WiFiUDP ntpUDP;
    NTPClient *mNTPClient;

    timer *EpochUpdater;

    time_t epochTime = 0;
    struct tm *ptm;

    int8_t mTimezone;
    String mNTP_Server;

   public:
    datetime(String ntpserver, int8_t timezone = Defaults.NTP.Timezone);

    bool Update();

    String Time(bool upd = false);
    String Date(bool upd = false);
    String DateTime(bool upd = false);
    int8_t Timezone() { return mTimezone; };
    void Timezone(int8_t tz);
    String NTP_Server() { return mNTP_Server; };

    void Control();
};

#endif