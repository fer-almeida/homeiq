#include "Network.h"

#include "Device.h"

network::network(device* device_obj) : Device(device_obj) {
    ConnectedEventHandler = WiFi.onStationModeConnected([&](const WiFiEventStationModeConnected& event) { mStatus = ConnectionStatus::Connected; });
    DisconnectedEventHandler = WiFi.onStationModeDisconnected([&](const WiFiEventStationModeDisconnected& event) { mStatus = ConnectionStatus::Disconnected; });

    Apply();
}

bool network::Apply() {
    bool ret = true;

    if (DHCP() == false) {
        if (!WiFi.config(StringToIPAddress(Device->Configuration->Setting["Network"]["IP Address"].as<String>()),
                         StringToIPAddress(Device->Configuration->Setting["Network"]["Gateway"].as<String>()),
                         StringToIPAddress(Device->Configuration->Setting["Network"]["Netmask"].as<String>()),
                         StringToIPAddress(Device->Configuration->Setting["Network"]["DNS Servers"][0].as<String>()),
                         StringToIPAddress(Device->Configuration->Setting["Network"]["DNS Servers"][1].as<String>()))) {
            Device->Log->Write("STA Failed to configure", LogTypes::Error);
        }
    }

    WiFi.disconnect();
    WiFi.softAPdisconnect();
    WiFi.begin(Device->Configuration->Setting["Network"]["SSID"].as<String>(), Device->Configuration->Setting["Network"]["PSK"].as<String>());
    WiFi.hostname(Device->Configuration->Setting["Network"]["Hostname"].as<String>().c_str());
    MDNS.begin(Device->Configuration->Setting["Network"]["Hostname"].as<String>().c_str());

    uint16_t connectionAttempt = 0;
    while (WiFi.status() != WL_CONNECTED) {
        if (connectionAttempt == Device->Configuration->Setting["Network"]["Connection Timeout"].as<uint16_t>()) break;
        delay(1000);
        connectionAttempt++;
    }

    if (WiFi.status() == WL_CONNECTED) {
        WiFi.mode(WIFI_STA);
        MDNS.addService("http", "tcp", Device->Configuration->Setting["Network"]["HTTP Port"].as<uint16_t>());
        mMode = APMode::WifiClient;
        ret = true;
    } else {
        Device->Log->Write("Unable to connect to " + Device->Configuration->Setting["Network"]["SSID"].as<String>() + " - Starting AP Mode", LogTypes::Warning);
        WiFi.mode(WIFI_AP);
        String tmpSSID = String(Device->Name() + "-" + MAC_Address().substring(MAC_Address().length() - 5, MAC_Address().length()));
        tmpSSID.replace(":", "");
        if (DHCP() == false) WiFi.softAPConfig(StringToIPAddress(Device->Configuration->Setting["Network"]["IP Address"].as<String>()), StringToIPAddress(Device->Configuration->Setting["Network"]["IP Address"].as<String>()), StringToIPAddress(Device->Configuration->Setting["Network"]["Netmask"].as<String>()));
        WiFi.softAP(tmpSSID, tmpSSID);
        mMode = APMode::SoftAP;
        ret = true;
    }

    return ret;
}

void network::Hostname(String value) {
    value.toLowerCase();
    Device->Configuration->Setting["Network"]["Hostname"] = value;
    Device->Configuration->SaveSettings();
}

String network::Hostname() {
    return WiFi.hostname();
}

void network::SSID(String value) {
    Device->Configuration->Setting["Network"]["SSID"] = value;
    Device->Configuration->SaveSettings();
}

String network::SSID() {
    if (mMode == APMode::WifiClient)
        return WiFi.SSID();
    else
        return WiFi.softAPSSID();
}

void network::PSK(String value) {
    Device->Configuration->Setting["Network"]["PSK"] = value;
    Device->Configuration->SaveSettings();
}

String network::PSK() {
    if (mMode == APMode::WifiClient)
        return WiFi.psk();
    else
        return WiFi.softAPPSK();
}

void network::IP_Address(IPAddress value) {
    Device->Configuration->Setting["Network"]["IP Address"] = value.toString();
    Device->Configuration->SaveSettings();
}

bool network::IP_Address(String value) {
    return IPV4_Set(value, IPV4::IPADDRESS);
}

IPAddress network::IP_Address() {
    if (mMode == APMode::WifiClient)
        return WiFi.localIP();
    else
        return WiFi.softAPIP();
}

void network::Gateway(IPAddress value) {
    Device->Configuration->Setting["Network"]["Gateway"] = value.toString();
    Device->Configuration->SaveSettings();
}

IPAddress network::Gateway() {
    if (mMode == APMode::WifiClient)
        return WiFi.gatewayIP();
    else
        return WiFi.softAPIP();
}

bool network::Gateway(String value) {
    return IPV4_Set(value, IPV4::GATEWAY);
}

void network::Netmask(IPAddress value) {
    Device->Configuration->Setting["Network"]["Netmask"] = value.toString();
    Device->Configuration->SaveSettings();
}

bool network::Netmask(String value) {
    return IPV4_Set(value, IPV4::MASK);
}

IPAddress network::Netmask() {
    if (mMode == APMode::WifiClient)
        return WiFi.subnetMask();
    else
        return StringToIPAddress(Device->Configuration->Setting["Network"]["Netmask"].as<String>());
}

void network::DHCP(bool value) {
    Device->Configuration->Setting["Network"]["DHCP"] = value;
    Device->Configuration->SaveSettings();
}

bool network::DHCP() {
    return Device->Configuration->Setting["Network"]["DHCP"].as<bool>();
}

void network::HTTP_Port(uint16_t value) {
    Device->Configuration->Setting["Network"]["HTTP Port"] = value;
    Device->Configuration->SaveSettings();
}

uint16_t network::HTTP_Port() {
    return Device->Configuration->Setting["Network"]["HTTP Port"].as<uint16_t>();
}

void network::HTTP_Enabled(bool value) {
    Device->Configuration->Setting["Network"]["HTTP Enabled"] = value;
    Device->Configuration->SaveSettings();
}

bool network::HTTP_Enabled() {
    return Device->Configuration->Setting["Network"]["HTTP Enabled"].as<bool>();
}

void network::Telnet_Port(uint16_t value) {
    Device->Configuration->Setting["Network"]["Telnet Port"] = value;
    Device->Configuration->SaveSettings();
}

uint16_t network::Telnet_Port() {
    return Device->Configuration->Setting["Network"]["Telnet Port"].as<uint16_t>();
}

void network::Telnet_Enabled(bool value) {
    Device->Configuration->Setting["Network"]["Telnet Enabled"] = value;
    Device->Configuration->SaveSettings();
}

bool network::Telnet_Enabled() {
    return Device->Configuration->Setting["Network"]["Telnet Enabled"].as<bool>();
}

void network::MQTT_Enabled(bool value) {
    Device->Configuration->Setting["Network"]["MQTT Enabled"] = value;
    Device->Configuration->SaveSettings();
}

bool network::MQTT_Enabled() {
    return Device->Configuration->Setting["Network"]["MQTT Enabled"].as<bool>();
}

void network::NTP_Server(String value) {
    Device->Configuration->Setting["Network"]["NTP Server"] = value;
    Device->Configuration->SaveSettings();
}

String network::NTP_Server() {
    return Device->Configuration->Setting["Network"]["NTP Server"].as<String>();
}

void network::NTP_Timezone(int8_t tz) {
    Device->DateTime->Timezone(tz);
    Device->Configuration->Setting["Network"]["Timezone"] = Device->DateTime->Timezone();
    Device->Configuration->SaveSettings();
}

int8_t network::NTP_Timezone() {
    return Device->DateTime->Timezone();
}

void network::DNS_Server(uint8_t index, String value) {
    Device->Configuration->Setting["Network"]["DNS Servers"][index] = value;
    Device->Configuration->SaveSettings();
}

void network::DNS_Server(uint8_t index, IPAddress value) {
    Device->Configuration->Setting["Network"]["DNS Servers"][index] = value.toString();
    Device->Configuration->SaveSettings();
}

IPAddress network::DNS_Server(uint8_t index = 0) {
    return WiFi.dnsIP(index);
}

void network::ConnectionTimeout(uint16_t value) {
    Device->Configuration->Setting["Network"]["Connection Timeout"] = value;
    Device->Configuration->SaveSettings();
}

uint16_t network::ConnectionTimeout() {
    return Device->Configuration->Setting["Network"]["Connection Timeout"].as<uint16_t>();
}

bool network::IPV4_Set(String value, IPV4 what) {
    IPAddress v;
    bool ret = v.fromString(value.c_str());

    if (ret) {
        switch (what) {
            case IPV4::IPADDRESS:
                IP_Address(v);
                break;
            case IPV4::MASK:
                Netmask(v);
                break;
            case IPV4::GATEWAY:
                Gateway(v);
                break;
        }
    }

    return ret;
}