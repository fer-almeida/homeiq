#include "Device.h"

device::device() {
    Start_EEPROM();
    Start_SerialPort();
    Start_FileSystem();
    Start_Configuration();
    Start_LogTool();
    Start_I2C();
    Start_Components();
    Start_Network();
    Start_RestartTimer();

    Components.Refresh();

    Log->Write("Ready!", LogTypes::Info);
}

void device::EEPROM_WriteInt8(uint16_t Address, int8_t Data) {
    EEPROM.write(Address, Data);
    EEPROM.commit();
}

int8_t device::EEPROM_ReadInt8(uint16_t Address) {
    return EEPROM.read(Address);
}

void device::EEPROM_WriteString(uint16_t Address, String Data) {
    uint8_t len = Data.length();
    for (uint8_t i = 0; i < len; i++)
        EEPROM.write(Address + i, Data[i]);
    EEPROM.write(Address + len, '\0');
    EEPROM.commit();
}

String device::EEPROM_ReadString(uint16_t Address, uint16_t MaxSize) {
    char Data[MaxSize + 1];
    uint8_t len = 0;
    char k;

    while (len < MaxSize) {
        k = EEPROM.read(Address + len);
        if (k == '\0')
            break;
        Data[len++] = k;
    }
    Data[len] = '\0';
    return String(Data);
}

String device::GetHardwareID() {
    String tmpID = String(ESP.getChipId(), HEX);
    tmpID.toUpperCase();

    uint16_t FlashSize = (ESP.getFlashChipRealSize() / 1000);

    return tmpID + " (" + String(ESP.getCpuFreqMHz()) + "MHz, " + (FlashSize > 1000 ? String(FlashSize / 1000) + "MB" : String(FlashSize) + "KB") + ")";
}

void device::Start_EEPROM() {
    EEPROM.begin(Defaults.EEPROM.Size);
}

void device::Start_SerialPort() {
    SerialPort = &Serial;
    SerialPort->begin(115200);
    SerialPort->println();

    SerialPort->print(String("\r\n::" + Version.ProductFamily + " " + Version.ProductName + " " + Version.Software.Info() + "\r\n\r\n"));
}

void device::Start_FileSystem() {
    FileSystem = new filesystem();
}

void device::Start_Configuration() {
    Configuration = new configuration(this);
}

void device::Start_LogTool() {
    Log = new logtool(this);

    String LogText = "Disabled";
    if (!Log->LogToNone()) {
        if (Log->LogToSerialPort())
            LogText = "Serial, ";
        if (Log->LogToSyslogServer())
            LogText += "Syslog (" + Configuration->Setting["Log"]["Syslog Server"].as<String>() + ":" + Configuration->Setting["Log"]["Syslog Port"].as<String>() + "), ";
        if (Log->LogToFile())
            LogText += "File, ";

        LogText = LogText.substring(0, LogText.length() - 2);
    }

    Log->Write("File System: " + String(FileSystem->TotalSpace()) + " bytes total, " + String(FileSystem->UsedSpace()) + " bytes in use (" + String(FileSystem->PercentUsed()) + "% used)", LogTypes::Info);
    Log->Write("Configuration: Settings read from " + Defaults.Files.Configuration, LogTypes::Info);
    Log->Write("Log Tool: " + LogText, LogTypes::Info);

    if (EEPROM_ReadInt8(Defaults.EEPROM.MapAddress.SerialNumber) == -1)
        Log->Write("Serial Number not set!", LogTypes::Error);
}

void device::Start_I2C() {
    I2C = new i2c(this);
    if (I2C->Enabled()) {
        if (I2C->Count() == 0) {
            Log->Write("I2C Bus: No I2C devices found", LogTypes::Error);
        } else {
            Log->Write("I2C Bus: " + String(I2C->Count()) + (I2C->Count() > 1 ? " devices found - " + I2C->Addresses() : " device found - " + I2C->Addresses()), LogTypes::Info);
        }
    } else {
        Log->Write("I2C Bus: Disabled", LogTypes::Info);
    }
}

void device::Start_Components() {
    for (uint16_t objs = 0; objs < Configuration->Setting["Components"].size(); objs++) {
        ComponentClasses NewComponent_Class = ComponentClasses::Generic;
        if (AvailableComponentClasses.find(Configuration->Setting["Components"][objs]["Class"].as<String>()) != AvailableComponentClasses.end())
            NewComponent_Class = AvailableComponentClasses.at(Configuration->Setting["Components"][objs]["Class"].as<String>());
        else
            continue;

        ComponentBuses NewComponent_Bus = ComponentBuses::Onboard;
        if (AvailableComponentBuses.find(Configuration->Setting["Components"][objs]["Bus"].as<String>().substring(0, Configuration->Setting["Components"][objs]["Bus"].as<String>().indexOf('('))) != AvailableComponentBuses.end())
            NewComponent_Bus = AvailableComponentBuses.at(Configuration->Setting["Components"][objs]["Bus"].as<String>().substring(0, Configuration->Setting["Components"][objs]["Bus"].as<String>().indexOf('(')));

        uint8_t NewComponent_BusAddress = Configuration->Setting["Components"][objs]["Bus"].as<String>().substring(Configuration->Setting["Components"][objs]["Bus"].as<String>().indexOf('(') + 1, Configuration->Setting["Components"][objs]["Bus"].as<String>().indexOf(')')).toInt();
        component *NewComponent = nullptr;

        switch (NewComponent_Class) {
            case ComponentClasses::Generic: {
            } break;
            case ComponentClasses::Blinds: {
                if (!((Components.IndexOf(Configuration->Setting["Components"][objs]["Relay Up"].as<String>()) > -1) && (Components[Configuration->Setting["Components"][objs]["Relay Up"].as<String>()]->Class() == ComponentClasses::Blinds)) && ((Components.IndexOf(Configuration->Setting["Components"][objs]["Relay Down"].as<String>()) > -1) && (Components[Configuration->Setting["Components"][objs]["Relay Down"].as<String>()]->Class() == ComponentClasses::Blinds))) continue;
                NewComponent = new blinds(this, objs, Configuration->Setting["Components"][objs]["Name"].as<String>(), (relay *)Components[Configuration->Setting["Components"][objs]["Relay Up"].as<String>()], (relay *)Components[Configuration->Setting["Components"][objs]["Relay Down"].as<String>()]);
                ((blinds *)NewComponent)->Position(Configuration->Setting["Components"][objs]["Position"].as<uint8_t>(), true);
                ((blinds *)NewComponent)->StepMs(Configuration->Setting["Components"][objs]["StepMs"].as<uint16_t>());
            } break;
            case ComponentClasses::Button: {
                NewComponent = new button(this, objs, Configuration->Setting["Components"][objs]["Name"].as<String>(), NewComponent_BusAddress, NewComponent_Bus);
            } break;
            case ComponentClasses::Currentmeter: {
            } break;
            case ComponentClasses::Relay: {
                NewComponent = new relay(this, objs, Configuration->Setting["Components"][objs]["Name"].as<String>(), NewComponent_BusAddress, NewComponent_Bus);
                ((relay *)NewComponent)->State(Configuration->Setting["Components"][objs]["State"].as<bool>());
            } break;
            case ComponentClasses::Thermometer: {
            } break;
        }

        JsonArray evts = Configuration->Setting["Components"][objs]["Events"].as<JsonArray>();
        for (JsonObject a : evts) {
            for (JsonPair kv : a) {
                for (auto e : NewComponent->Event) {
                    if (e.first.equalsIgnoreCase(kv.key().c_str())) {
                        String Event_Action = kv.value().as<String>();
                        String Action_Command = Event_Action.substring(0, Event_Action.indexOf('('));
                        String Action_Parameter = Event_Action.substring(Action_Command.length() + 1, Event_Action.indexOf(')'));

                        // Replace static variables
                        Action_Parameter.replace("%NAME%", NewComponent->Name());

                        // Tokenize parameters
                        std::vector<String> Tokenized_Action_Parameters = SString(Action_Parameter).Tokenize(',');

                        // Log
                        if (Action_Command.equalsIgnoreCase("log")) {
                            NewComponent->Event[e.first]([&, Action_Parameter, Self = NewComponent] {
                                // Replace dynamic variables
                                String tmpAction_Parameter = Action_Parameter;

                                tmpAction_Parameter.replace("%RELAYSTATE%", (((relay *)Self)->State() ? "true" : "false"));

                                Log->Write(tmpAction_Parameter, LogTypes::Info);
                            });
                        }

                        // Invert
                        if (Action_Command.equalsIgnoreCase("invert")) {
                            NewComponent->Event[e.first]([&, Action_Parameter] {
                                if (Components.IndexOf(Action_Parameter) > -1) {
                                    if (Components[Action_Parameter]->Class() == ComponentClasses::Relay) {
                                        ((relay *)Components[Action_Parameter])->Invert();
                                    }
                                }
                            });
                        }

                        // State
                        if (Action_Command.equalsIgnoreCase("state")) {
                            NewComponent->Event[e.first]([&, Tokenized_Action_Parameters] {
                                if (Tokenized_Action_Parameters.size() == 2) {  // Two parameters needed
                                    if (Components[Tokenized_Action_Parameters[0]]->Class() == ComponentClasses::Relay) {
                                        ((relay *)Components[Tokenized_Action_Parameters[0]])->State((bool)Tokenized_Action_Parameters[1].toInt());
                                    }
                                }
                            });
                        }

                        // Open
                        if (Action_Command.equalsIgnoreCase("open")) {
                            NewComponent->Event[e.first]([&, Action_Parameter] {
                                if (Components.IndexOf(Action_Parameter) > -1) {
                                    if (Components[Action_Parameter]->Class() == ComponentClasses::Blinds) {
                                        ((blinds *)Components[Action_Parameter])->Open();
                                    }
                                }
                            });
                        }

                        // Close
                        if (Action_Command.equalsIgnoreCase("close")) {
                            NewComponent->Event[e.first]([&, Action_Parameter] {
                                if (Components.IndexOf(Action_Parameter) > -1) {
                                    if (Components[Action_Parameter]->Class() == ComponentClasses::Blinds) {
                                        ((blinds *)Components[Action_Parameter])->Close();
                                    }
                                }
                            });
                        }

                        // Position
                        if (Action_Command.equalsIgnoreCase("position")) {
                            NewComponent->Event[e.first]([&, Tokenized_Action_Parameters] {
                                if (Tokenized_Action_Parameters.size() == 2) {  // Two parameters needed
                                    if (Components[Tokenized_Action_Parameters[0]]->Class() == ComponentClasses::Blinds) {
                                        ((blinds *)Components[Tokenized_Action_Parameters[0]])->Position(Tokenized_Action_Parameters[1].toInt());
                                    }
                                }
                            });
                        }
                    }
                }
            }
        }
        Components.Add(NewComponent);
    }

    if (Components.Count() == 0)
        Log->Write("Components: No components added", LogTypes::Warning);
    else
        Log->Write("Components: Added " + String(Components.Count()) + " component(s)", LogTypes::Info);
}

void device::Start_Network() {
    Network = new network(this);

    if (Network->Mode() != APMode::Offline) {
        // DateTime
        DateTime = new datetime(Configuration->Setting["Network"]["NTP Server"].as<String>(), Configuration->Setting["Network"]["Timezone"].as<int8_t>());
        if (DateTime->Update()) {
            Log->Write("DateTime: Updated date and time, NTP server '" + DateTime->NTP_Server() + "' GMT " + String((DateTime->Timezone() > 0 ? "+" : "") + String(DateTime->Timezone())) + " " + DateTime->DateTime(), LogTypes::Info);
        } else {
            Log->Write("DateTime: Unable to update date and time, NTP server '" + DateTime->NTP_Server() + "' GMT " + String((DateTime->Timezone() > 0 ? "+" : "") + String(DateTime->Timezone())) + " " + DateTime->DateTime(), LogTypes::Error);
        }

        if (Network->HTTP_Enabled()) {
            WebServer = new AsyncWebServer(Network->HTTP_Port());

            // Static Content
            // WebServer->on("/res/css/default.css", HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content("/res/css/default.css", "text/css", request, false, true); });
            // WebServer->on("/res/css/styles.css", HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content("/res/css/styles.css", "text/css", request, false, true); });
            // WebServer->on("/res/img/logo-w.png", HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content("/res/img/logo-w.png", "image/png", request, false, true); });
            // WebServer->on("/res/img/esp8266.png", HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content("/res/img/esp8266.png", "image/png", request, false, true); });
            // WebServer->on(Defaults.Files.Log.c_str(), HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content(Defaults.Files.Log, "text/plain", request, true, true); });
            WebServer->on(Defaults.Files.Configuration.c_str(), HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content(Defaults.Files.Configuration, "text/plain", request, true, true); });

            // // Dynamic Content
            // WebServer->on("/", HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content("/index.html", "text/html", request, true); });
            // WebServer->on("/index.html", HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content("/index.html", "text/html", request, true); });
            // WebServer->on("/login.html", HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content("/login.html", "text/html", request, false); });
            // WebServer->on("/logview.html", HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content("/logview.html", "text/html", request, true); });
            // WebServer->on("/config.html", HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content("/config.html", "text/html", request, true); });
            // WebServer->on("/about.html", HTTP_GET, [&](AsyncWebServerRequest *request) { Web_Content("/about.html", "text/html", request, true); });
            // WebServer->on("/upload.cgi", HTTP_POST, [&](AsyncWebServerRequest *request) {}, [&](AsyncWebServerRequest *request, const String &filename, size_t index, uint8_t *data, size_t len, bool final) { Web_Upload(request, filename, index, data, len, final); });
            // WebServer->on("/applyupdate.cgi", HTTP_POST, [&](AsyncWebServerRequest *request) {}, [&](AsyncWebServerRequest *request, const String &filename, size_t index, uint8_t *data, size_t len, bool final) { Web_ApplyUpdate(request, filename, index, data, len, final, true); });

            // WebServer->on("/login.cgi", HTTP_POST, [&](AsyncWebServerRequest *request) {
            //     String msg;

            //     if (request->hasArg("username") && request->hasArg("password")) {
            //         uint16_t UserIndex = Configuration->IndexOf_User((request->arg("username")));
            //         if (UserIndex > -1) {
            //             if (request->arg("username") == Configuration->Setting["Security"]["Users"][UserIndex]["Name"] && request->arg("password") == Configuration->Setting["Security"]["Users"][UserIndex]["Password"]) {
            //                 AsyncWebServerResponse *response = request->beginResponse(301);  // Sends 301 redirect

            //                 response->addHeader("Location", "/");
            //                 response->addHeader("Cache-Control", "no-cache");

            //                 String token = String(request->arg("username") + ":" + request->arg("password") + ":" + request->client()->remoteIP().toString());
            //                 response->addHeader("Set-Cookie", "ESPSESSIONID=" + token);

            //                 request->send(response);

            //                 Log->Write("HTTP logon successful for '" + request->arg("username") + "@" + request->client()->remoteIP().toString() + "'", LogTypes::Info);
            //                 return;
            //             }
            //             msg = "You do not have permissions to access this device";
            //             Log->Write("HTTP login failed for '" + request->arg("username") + "@" + request->client()->remoteIP().toString() + "'", LogTypes::Warning);
            //             AsyncWebServerResponse *response = request->beginResponse(301);  // Sends 301 redirect

            //             response->addHeader("Location", "/login.html?msg=" + msg);
            //             response->addHeader("Cache-Control", "no-cache");
            //             request->send(response);
            //             return;
            //         }
            //     }
            // });

            // WebServer->on("/config.cgi", HTTP_GET, [&](AsyncWebServerRequest *request) {
            //     // Web_HandleAuthentication(request);

            //     if (request->hasArg("cmd")) {
            //         String cmd, params;

            //         cmd = request->arg("cmd");
            //         params = (request->hasArg("params") ? request->arg("params") : "");

            //         if (cmd.equalsIgnoreCase("log")) {
            //             if (params.equalsIgnoreCase("clear")) {
            //                 if (Configuration->Admin_User(Web_GetSessionCookieValue(request, 1))) {
            //                     Log->Clear();
            //                 } else {
            //                     Log->Write("Log clear failed for '" + Web_GetSessionCookieValue(request, 1) + "@" + request->client()->remoteIP().toString() + "'", LogTypes::Warning);
            //                 }
            //                 AsyncWebServerResponse *response = request->beginResponse(301);  // Sends 301 redirect
            //                 response->addHeader("Location", "/logview.html");
            //                 response->addHeader("Cache-Control", "no-cache");
            //                 request->send(response);
            //             }
            //         }

            //         if (cmd.equalsIgnoreCase("logout")) {
            //             Log->Write("HTTP logout for '" + Web_GetSessionCookieValue(request, 1) + "@" + request->client()->remoteIP().toString() + "'", LogTypes::Info);

            //             AsyncWebServerResponse *response = request->beginResponse(301);  // Sends 301 redirect
            //             response->addHeader("Location", "/login.html");
            //             response->addHeader("Cache-Control", "no-cache");
            //             response->addHeader("Set-Cookie", "ESPSESSIONID=0");
            //             request->send(response);
            //         }
            //     }
            // });

            WebServer->onNotFound([](AsyncWebServerRequest *request) { request->send(404); });

            // Components links
            // for (auto m : Components) {
            //     switch (m->Class()) {
            //         case ComponentClasses::Button:
            //             WebServer->on(String("/" + m->Name()).c_str(), HTTP_GET, [this, mdl = (button *)m](AsyncWebServerRequest *request) {
            //                 HTTPJSON_HandleAuthentication(request);

            //                 request->send(200, "application/json", "{\"Name\":\"" + mdl->Name() + "\",\r\n\"IsPressed\":" + String(mdl->IsPressed()) + "}");
            //                 Log->Write("HTTP JSON get " + mdl->Name() + " sent to " + request->client()->remoteIP().toString(), LogTypes::Info);
            //             });
            //             break;
            //         case ComponentClasses::Relay:
            //             WebServer->on(String("/" + m->Name()).c_str(), HTTP_GET, [this, mdl = (relay *)m](AsyncWebServerRequest *request) {
            //                 HTTPJSON_HandleAuthentication(request);

            //                 bool isSet = false;
            //                 if (request->args() > 0) {
            //                     for (uint8_t i = 0; i < (uint8_t)request->args(); i++) {
            //                         String Arg = request->argName(i);
            //                         String Val = request->arg(i);

            //                         if (Arg.equalsIgnoreCase("state")) {
            //                             if (Val != "?") {
            //                                 isSet = true;
            //                                 mdl->State((bool)constrain(Val.toInt(), 0, 1));
            //                             }
            //                         }
            //                     }
            //                 }

            //                 request->send(200, "application/json", "{\"Name\":\"" + mdl->Name() + "\",\r\n\"State\":" + String(mdl->State()) + "}");
            //                 Log->Write("HTTP JSON " + String(isSet ? "s" : "g") + "et " + mdl->Name() + String(isSet ? " from " : " sent to ") + request->client()->remoteIP().toString(), LogTypes::Info);
            //             });
            //             break;
            //         case ComponentClasses::Thermometer:
            //             WebServer->on(String("/" + m->Name()).c_str(), HTTP_GET, [this, mdl = (thermometer *)m](AsyncWebServerRequest *request) {
            //                 HTTPJSON_HandleAuthentication(request);

            //                 request->send(200, "application/json", "{\"Name\":\"" + mdl->Name() + "\",\r\n\"Temperature\":" + String(mdl->Temperature()) + ",\r\n\"Humidity\":" + String(mdl->Humidity()) + "}");
            //                 Log->Write("HTTP JSON get " + mdl->Name() + " sent to " + request->client()->remoteIP().toString(), LogTypes::Info);
            //             });
            //             break;
            //         case ComponentClasses::Currentmeter:
            //             WebServer->on(String("/" + m->Name()).c_str(), HTTP_GET, [this, mdl = (currentmeter *)m](AsyncWebServerRequest *request) {
            //                 HTTPJSON_HandleAuthentication(request);

            //                 request->send(200, "application/json", "{\"Name\":\"" + mdl->Name() + "\",\r\n\"CurrentAC\":" + String(mdl->CurrentAC()) + ",\r\n\"CurrentDC\":" + String(mdl->CurrentDC()) + "}");
            //                 Log->Write("HTTP JSON get " + mdl->Name() + " sent to " + request->client()->remoteIP().toString(), LogTypes::Info);
            //             });
            //             break;
            //         default:
            //             break;
            //     }
            // }

            WebServer->begin();
        }
        Log->Write("HTTP Server: " + String(Network->HTTP_Enabled() ? "Enabled" : "Disabled") + " on TCP port " + String(Network->HTTP_Port()), LogTypes::Info);

        // Telnet Server
        if (Network->Telnet_Enabled()) {
            TelnetServer = new telnetserver(this);
        }
        Log->Write("Telnet Server: " + String(Network->Telnet_Enabled() ? "Enabled" : "Disabled") + " on TCP port " + String(Network->Telnet_Port()), LogTypes::Info);

        // MQTT
        if (Network->MQTT_Enabled()) {
            MQTT = new mqtt(this);
        }
        Log->Write("MQTT: " + String(Network->MQTT_Enabled() ? "Enabled" : "Disabled") + " | ID " + Network->Hostname() + " - Broker: " + MQTT->Broker_URL() + ":" + String(MQTT->Broker_Port()) + " - User: [" + MQTT->User() + "] - Password: [" + MQTT->Password() + "]", LogTypes::Info);

        switch (Network->Mode()) {
            case APMode::WifiClient: {
                Log->Write("Network: Client Mode | Connected to '" + Network->SSID() + "' - (IP Address " + Network->IP_Address().toString() + " / MAC Address: " + Network->MAC_Address() + ")", LogTypes::Info);
            } break;
            case APMode::SoftAP: {
                Log->Write("Network: AP Mode | Connected to '" + Network->SSID() + "' - (IP Address " + Network->IP_Address().toString() + " / MAC Address: " + Network->MAC_Address() + ")", LogTypes::Warning);
            } break;
            default: {
                Log->Write("Network: No network connection", LogTypes::Error);
            } break;
        }
    }
}

void device::Start_RestartTimer() {
    String RestartWhenStr;

    switch (RestartWhen()) {
        case APMode::SoftAP: {
            RestartWhenStr = "when on SoftAP mode";
        } break;
        case APMode::Offline: {
            RestartWhenStr = "when on offline/no connection mode";
        } break;
        case APMode::WifiClient: {
            RestartWhenStr = "when on WiFi client mode";
        } break;
        default: {
            RestartWhenStr = "in any mode";
        } break;
    }

    Log->Write("Restart Timer: " + String(RestartInterval() > 0 ? "Enabled - Every " + String(RestartInterval()) + " seconds, " + RestartWhenStr : "Disabled"), LogTypes::Info);

    RestartIntervalTimer = new timer(RestartInterval() * 1000);
    RestartIntervalTimer->OnTimeout([&] {
        if (RestartWhen() == Network->Mode()) {
            Log->Write("Reached restart interval (" + String(RestartInterval()) + ") seconds. Restarting Device", LogTypes::Info);
            Restart();
        }
    });
    RestartIntervalTimer->Start();
}

bool device::HTTPJSON_IsAuthenticated(AsyncWebServerRequest *request) {
    bool isAuth = false;
    uint16_t objs = 0;

    for (objs = 0; objs < Configuration->Setting["Security"]["Users"].size(); objs++) {
        if (!request->authenticate(Configuration->Setting["Security"]["Users"][objs]["Name"].as<String>().c_str(), Configuration->Setting["Security"]["Users"][objs]["Password"].as<String>().c_str())) {
            isAuth = false;
        } else {
            isAuth = true;
            break;
        }
    }

    return isAuth;
}

bool device::Web_IsAuthenticated(AsyncWebServerRequest *request) {
    if (request->hasHeader("Cookie")) {
        String cookie = request->header("Cookie");

        uint16_t UserIndex = Configuration->IndexOf_User(Web_GetSessionCookieValue(request, 1));
        if (UserIndex > -1) {
            String token = String(Configuration->Setting["Security"]["Users"][UserIndex]["Name"].as<String>() + ":" + Configuration->Setting["Security"]["Users"][UserIndex]["Password"].as<String>() + ":" + request->client()->remoteIP().toString());
            if (cookie.indexOf("ESPSESSIONID=" + token) != -1)
                return true;
        }
    }
    return false;
}

String device::Web_GetSessionCookieValue(AsyncWebServerRequest *request, uint16_t index) {
    if (request->hasHeader("Cookie")) {
        String cookie = request->header("Cookie");

        if (cookie.substring(0, 13).equalsIgnoreCase("espsessionid=")) {
            cookie = cookie.substring(14);

            std::vector<String> CookieValues = SString(cookie).Tokenize(':');
            if (CookieValues.size() > 0) return CookieValues[index];
        }
    }
    return "";
}

void device::HTTPJSON_HandleAuthentication(AsyncWebServerRequest *request) {
    if (!HTTPJSON_IsAuthenticated(request)) {
        request->requestAuthentication();
    }
}

void device::Web_HandleAuthentication(AsyncWebServerRequest *request) {
    if (!Web_IsAuthenticated(request)) {
        AsyncWebServerResponse *response = request->beginResponse(301);  // Sends 301 redirect
        response->addHeader("Location", "/login.html");
        response->addHeader("Cache-Control", "no-cache");
        request->send(response);
    }
}

void device::Web_Content(String content, String mimetype, AsyncWebServerRequest *request, bool requires_authentication, bool static_content) {
    // if (requires_authentication) Web_HandleAuthentication(request);

    if (static_content) {
        request->send(LittleFS, content, mimetype, false);
    } else {
        request->send(LittleFS, content, mimetype, false, [&](const String &var) {
            if (var.equalsIgnoreCase("sw_version")) {
                return Version.Software.Info();
            }
            if (var.equalsIgnoreCase("hw_version")) {
                return Version.Hardware.Info();
            }
            if (var.equalsIgnoreCase("productname")) {
                return Version.ProductName;
            }
            if (var.equalsIgnoreCase("devicename")) {
                return Name();
            }
            if (var.equalsIgnoreCase("hardware")) {
                return Version.Hardware.Model;
            }
            if (var.equalsIgnoreCase("serial")) {
                return GetSerialNumber();
            }
            if (var.equalsIgnoreCase("chip_id")) {
                return GetHardwareID();
            }
            if (var.equalsIgnoreCase("config_file")) {
                return Defaults.Files.Configuration;
            }
            if (var.equalsIgnoreCase("log_file")) {
                return Defaults.Files.Log;
            }
            if (var.equalsIgnoreCase("ram")) {
                return String(ESP.getFreeHeap()) + " bytes free";
            }
            if (var.equalsIgnoreCase("file_system")) {
                return String(FileSystem->TotalSpace() / 1000) + " KB total, " + String(FileSystem->UsedSpace() / 1000) + " KB in use (" + String(FileSystem->PercentUsed()) + "%% used)";
            }
            if (var.equalsIgnoreCase("wifi_status")) {
                return Network->SSID() + " (" + (Network->Mode() == APMode::WifiClient ? "Client" : "AP") + " Mode, " + String(WiFi.RSSI()) + " dBm RSSI)";
            }
            if (var.equalsIgnoreCase("hostname")) {
                return Network->Hostname();
            }
            if (var.equalsIgnoreCase("ssid")) {
                return Network->SSID();
            }
            if (var.equalsIgnoreCase("psk")) {
                return Network->PSK();
            }
            if (var.equalsIgnoreCase("ip1")) {
                return String(Network->IP_Address()[0]);
            }
            if (var.equalsIgnoreCase("ip2")) {
                return String(Network->IP_Address()[1]);
            }
            if (var.equalsIgnoreCase("ip3")) {
                return String(Network->IP_Address()[2]);
            }
            if (var.equalsIgnoreCase("ip4")) {
                return String(Network->IP_Address()[3]);
            }
            if (var.equalsIgnoreCase("gw1")) {
                return String(Network->Gateway()[0]);
            }
            if (var.equalsIgnoreCase("gw2")) {
                return String(Network->Gateway()[1]);
            }
            if (var.equalsIgnoreCase("gw3")) {
                return String(Network->Gateway()[2]);
            }
            if (var.equalsIgnoreCase("gw4")) {
                return String(Network->Gateway()[3]);
            }
            if (var.equalsIgnoreCase("ms1")) {
                return String(Network->Netmask()[0]);
            }
            if (var.equalsIgnoreCase("ms2")) {
                return String(Network->Netmask()[1]);
            }
            if (var.equalsIgnoreCase("ms3")) {
                return String(Network->Netmask()[2]);
            }
            if (var.equalsIgnoreCase("ms4")) {
                return String(Network->Netmask()[3]);
            }
            if (var.equalsIgnoreCase("ns1_1")) {
                return String(Network->DNS_Server(0)[0]);
            }
            if (var.equalsIgnoreCase("ns1_2")) {
                return String(Network->DNS_Server(0)[1]);
            }
            if (var.equalsIgnoreCase("ns1_3")) {
                return String(Network->DNS_Server(0)[2]);
            }
            if (var.equalsIgnoreCase("ns1_4")) {
                return String(Network->DNS_Server(0)[3]);
            }
            if (var.equalsIgnoreCase("ns2_1")) {
                return String(Network->DNS_Server(1)[0]);
            }
            if (var.equalsIgnoreCase("ns2_2")) {
                return String(Network->DNS_Server(1)[1]);
            }
            if (var.equalsIgnoreCase("ns2_3")) {
                return String(Network->DNS_Server(1)[2]);
            }
            if (var.equalsIgnoreCase("ns2_4")) {
                return String(Network->DNS_Server(1)[3]);
            }
            if (var.equalsIgnoreCase("dhcp_client")) {
                return String(Network->DHCP() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("http_port")) {
                return String(Network->HTTP_Port());
            }
            if (var.equalsIgnoreCase("http_enabled")) {
                return String(Network->HTTP_Enabled() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("telnet_port")) {
                return String(Network->Telnet_Port());
            }
            if (var.equalsIgnoreCase("telnet_enabled")) {
                return String(Network->Telnet_Enabled() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("dns_primary")) {
                return Network->DNS_Server(0).toString();
            }
            if (var.equalsIgnoreCase("dns_secondary")) {
                return Network->DNS_Server(1).toString();
            }
            if (var.equalsIgnoreCase("connection_timeout")) {
                return String(Network->ConnectionTimeout());
            }
            if (var.equalsIgnoreCase("mac_address")) {
                return Network->MAC_Address();
            }
            if (var.equalsIgnoreCase("i2c_enabled")) {
                return String(I2C->Enabled() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("restart_interval")) {
                return String(RestartInterval());
            }
            if (var.equalsIgnoreCase("log_info")) {
                return String(Log->LogLevelInfo() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("log_warn")) {
                return String(Log->LogLevelWarnings() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("log_error")) {
                return String(Log->LogLevelErrors() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("log_dbg")) {
                return String(Log->LogLevelDebug() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("logto_serial")) {
                return String(Log->LogToSerialPort() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("logto_file")) {
                return String(Log->LogToFile() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("logto_syslog")) {
                return String(Log->LogToFile() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("syslog_server")) {
                return Configuration->Setting["Log"]["Syslog Server"].as<String>();
            }
            if (var.equalsIgnoreCase("syslog_port")) {
                return Configuration->Setting["Log"]["Syslog Port"].as<String>();
            }
            if (var.equalsIgnoreCase("mqtt_enabled")) {
                return String(Network->MQTT_Enabled() ? "checked" : "");
            }
            if (var.equalsIgnoreCase("mqtt_port")) {
                return String(MQTT->Broker_Port());
            }
            if (var.equalsIgnoreCase("mqtt_brokerurl")) {
                return MQTT->Broker_URL();
            }
            if (var.equalsIgnoreCase("mqtt_user")) {
                return MQTT->User();
            }
            if (var.equalsIgnoreCase("mqtt_password")) {
                return MQTT->Password();
            }

            return String();
        });
    }

    Log->Write("HTTP Server: " + content + " sent to " + request->client()->remoteIP().toString(), LogTypes::Debug);
}

void device::Web_Upload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final) {
    struct uploadRequest {
        uploadRequest *next;
        AsyncWebServerRequest *request;
        File uploadFile;
        uint32_t fileSize;
        uploadRequest() {
            next = nullptr;
            request = nullptr;
            fileSize = 0;
        }
    };

    static uploadRequest uploadRequestHead;
    uploadRequest *thisUploadRequest = nullptr;

    if (!index) {
        String toFile = filename;

        if (!toFile.startsWith("/"))
            toFile = "/" + toFile;
        if (LittleFS.exists(toFile))
            LittleFS.remove(toFile);

        thisUploadRequest = new uploadRequest;
        thisUploadRequest->request = request;
        thisUploadRequest->next = uploadRequestHead.next;
        uploadRequestHead.next = thisUploadRequest;
        thisUploadRequest->uploadFile = LittleFS.open(toFile, "w");
        Serial.println("Upload: START, filename: " + toFile);
    } else {
        thisUploadRequest = uploadRequestHead.next;
        while (thisUploadRequest->request != request)
            thisUploadRequest = thisUploadRequest->next;
    }

    if (thisUploadRequest->uploadFile) {
        for (size_t i = 0; i < len; i++) {
            thisUploadRequest->uploadFile.write(data[i]);
        }
        thisUploadRequest->fileSize += len;
    }

    if (final) {
        thisUploadRequest->uploadFile.close();
        Serial.print("Upload: END, Size: ");
        Serial.println(thisUploadRequest->fileSize);
        // uploadRequest* linkUploadRequest = &uploadRequestHead;
        // while(linkUploadRequest->next != thisUploadRequest) linkUploadRequest = linkUploadRequest->next;
        // linkUploadRequest->next = thisUploadRequest->next;
        // delete thisUploadRequest;
        request->redirect("/");
    }
}

void device::Web_ApplyUpdate(AsyncWebServerRequest *request, const String &filename, size_t index, uint8_t *data, size_t len, bool final, bool requires_authentication) {
    size_t content_len;

    if (!index) {
        Log->Write("Firmware update started", LogTypes::Info);
        content_len = request->contentLength();

        int cmd = (filename.indexOf("littlefs") > -1) ? U_FS : U_FLASH;

        Update.runAsync(true);
        if (!Update.begin(content_len, cmd))
            Update.printError(Serial);
    }

    if (Update.write(data, len) != len)
        Update.printError(Serial);
    // else Serial.printf("Progress: %d%%\n", (Update.progress()*100) / Update.size());

    if (final) {
        AsyncWebServerResponse *response = request->beginResponse(302, "text/plain", "Please wait while the device reboots");
        response->addHeader("Refresh", "20");
        response->addHeader("Location", "/");
        request->send(response);

        if (!Update.end(true)) {
            Log->Write("Error while updating firmware", LogTypes::Error);
        } else {
            Log->Write("Firmware update finished successful", LogTypes::Info);
            ESP.restart();
        }
    }
}

String device::TimeStamp() {
    if (DateTime == nullptr) {
        char ts[22];
        sprintf(ts, "0000-00-00 %08ld", millis());
        return String("[" + String(ts) + "] ");
    }
    return String("[" + DateTime->DateTime() + "] ");
};

void device::RestartWhen(APMode when) {
    switch (when) {
        case APMode::SoftAP: {
            Configuration->Setting["General"]["Restart When"] = "SoftAP";
        } break;
        case APMode::Offline: {
            Configuration->Setting["General"]["Restart When"] = "Offline";
        } break;
        case APMode::WifiClient: {
            Configuration->Setting["General"]["Restart When"] = "WiFiClient";
        } break;
        default: {
            Configuration->Setting["General"]["Restart When"] = "Always";
        } break;
    }
    Configuration->SaveSettings();
}

APMode device::RestartWhen() {
    if (Configuration->Setting["General"]["Restart When"].as<String>().equalsIgnoreCase("softap"))
        return APMode::SoftAP;
    else if (Configuration->Setting["General"]["Restart When"].as<String>().equalsIgnoreCase("offline"))
        return APMode::Offline;
    else if (Configuration->Setting["General"]["Restart When"].as<String>().equalsIgnoreCase("wificlient"))
        return APMode::WifiClient;
    else
        return APMode::None;
}

void device::Control() {
    if (Network->Status() == ConnectionStatus::Connected) {
        if (Network->Mode() == APMode::WifiClient)
            MQTT->Control();
    }
    if (RestartInterval() > 0)
        RestartIntervalTimer->Control();

    DateTime->Control();
    Components.Control();
}