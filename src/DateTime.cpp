#include "DateTime.h"

datetime::datetime(String ntpserver, int8_t timezone) : mTimezone(timezone) {
    mNTP_Server = (ntpserver.equalsIgnoreCase("null") ? Defaults.NTP.NTPServer : ntpserver);

    mNTPClient = new NTPClient(ntpUDP, mNTP_Server.c_str());
    mNTPClient->begin();
    Timezone(timezone);

    EpochUpdater = new timer(1000);
    EpochUpdater->OnTimeout([&] { epochTime++; });
    EpochUpdater->Start();
}

bool datetime::Update() {
    bool ret = false;
    if (mNTPClient != nullptr) {
        ret = mNTPClient->update();
        epochTime = mNTPClient->getEpochTime();
        if (!ret) epochTime = 1704067200;
    }
    return ret;
}

void datetime::Timezone(int8_t tz) {
    mTimezone = constrain(tz, -12, 14);
    mNTPClient->setTimeOffset(mTimezone * 3600);
}

String datetime::Time(bool upd) {
    if (upd) Update();
    ptm = gmtime((time_t *)&epochTime);

    char str[9];
    sprintf(str, "%02u:%02u:%02u", ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
    return String(str);
}

String datetime::Date(bool upd) {
    if (upd) Update();
    ptm = gmtime((time_t *)&epochTime);

    char str[11];
    sprintf(str, "%04u-%02u-%02u", ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday);
    return String(str);
}

String datetime::DateTime(bool upd) {
    return String(Date(upd) + " " + Time());
}

void datetime::Control() {
    EpochUpdater->Control();
}