#include "Relay.h"

#include "Device.h"

relay::relay(device *device_obj, uint16_t id, String name, uint8_t busaddress, ComponentBuses bus, RelayTypes relaytype) : component(name, id, busaddress, bus), Device(device_obj), mRelayType(relaytype) {
    Event.insert({{"SettingOn", [&](callback_t callback) { mSettingOnCallback = callback; }},
                  {"SettingOff", [&](callback_t callback) { mSettingOffCallback = callback; }},
                  {"SetOn", [&](callback_t callback) { mSetOnCallback = callback; }},
                  {"SetOff", [&](callback_t callback) { mSetOff = callback; }},
                  {"Changed", [&](callback_t callback) { mChangedCallback = callback; }}});

    switch (Bus()) {
        case ComponentBuses::I2C: {
            pcf8574 = new PCF8574(mBusAddress);
            pcf8574->begin();
        } break;
        default: {
            pinMode(mBusAddress, OUTPUT);
        } break;
    }
}

const bool relay::State(bool newstate, bool savestate) {
    mState = (mRelayType == RelayTypes::NormallyOpened ? !newstate : newstate);

    if (newstate) {
        if (mSettingOnCallback) mSettingOnCallback();
    } else {
        if (mSettingOffCallback) mSettingOffCallback();
    }

    switch (Bus()) {
        case ComponentBuses::I2C: {
            pcf8574->write(mBusAddress, mState);
        } break;
        default: {
            digitalWrite(mBusAddress, mState);
        } break;
    }

    if (savestate) {
        Device->Configuration->Setting["Components"][mID]["State"] = State();
        Device->Configuration->SaveSettings();
    }

    if (Device->MQTT != nullptr) Device->MQTT->Publish(String("Get/Relay:" + mName), (State() ? "1" : "0"));

    if (newstate) {
        if (mSetOnCallback) mSetOnCallback();
    } else {
        if (mSetOff) mSetOff();
    }

    if (mChangedCallback) mChangedCallback();

    return State();
}

void relay::Refresh() {
    if (Device->MQTT != nullptr) Device->MQTT->Publish(String("Get/Relay:" + mName), (State() ? "1" : "0"));
}