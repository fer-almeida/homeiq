#include "Timer.h"

void timer::Control() {
    if (mState == Timer_State::TimerRunning && mTimeout > 0 && CurrentTimerMs() >= mTimeout) {
        Reset();
        if (mOnTimeoutCallback) mOnTimeoutCallback();
    }
}