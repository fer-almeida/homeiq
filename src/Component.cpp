#include "Component.h"

int16_t components::IndexOf(String name) {
    for (const auto& c : mComponents)
        if (c->Name().equalsIgnoreCase(name)) return c->ID();
    return -1;
}

bool components::Add(component* new_Component) {
    if (IndexOf(new_Component->Name()) == -1) {
        bool exists = false;

        for (const auto& c : mComponents) {
            switch (c->Class()) {
                case ComponentClasses::Generic: {
                } break;
                case ComponentClasses::Blinds: {                    
                } break;
                case ComponentClasses::Button:                
                case ComponentClasses::Currentmeter:
                case ComponentClasses::Relay:
                case ComponentClasses::Thermometer: {
                    if ((c->Bus() == new_Component->Bus()) && (c->BusAddress() == new_Component->BusAddress())) exists = true;
                } break;
            }
        }

        if (!exists) {
            mComponents.push_back(new_Component);
            return true;
        }
    }
    return false;
}

int16_t components::Remove(int16_t index) {
    if (index > (int16_t)(mComponents.size() - 1)) return -1;

    mComponents.erase(begin() + index);
    return mComponents.size();
}

int16_t components::Remove(String name) {
    return ((IndexOf(name) > -1) ? Remove(IndexOf(name)) : -1);
}

int16_t components::Clear() {
    for (auto& c : mComponents) {
        delete c;
        c = nullptr;
    }
    mComponents.clear();
    return 0;
}

component* components::At(int16_t index) {
    if (index > (int16_t)(mComponents.size() - 1)) return nullptr;
    return mComponents[index];
}