#include "Thermometer.h"

#include "Device.h"

thermometer::thermometer(device *device_obj, uint16_t id, String name, uint8_t busaddress, ComponentBuses bus, ThermometerTypes type, TemperatureScales scale) : component(name, id, busaddress, bus), Device(device_obj), mThermometerType(type), Scale(scale) {
    Event.insert({
        {"TemperatureChanged", [&](callback_t callback) { mTemperatureChanged = callback; }},
        {"HumidityChanged", [&](callback_t callback) { mHumidityChanged = callback; }},
    });

    Updater = new timer(Defaults.UpdateRefreshMs);

    switch (mThermometerType) {
        case ThermometerTypes::DHT_11:
        case ThermometerTypes::DHT_12:
        case ThermometerTypes::DHT_21:
        case ThermometerTypes::DHT_22: {
            dht = new DHT_Unified(mBusAddress, mThermometerType);
            dht->begin();

            sensor_t sensor;
            dht->temperature().getSensor(&sensor);

            Updater->OnTimeout([&] {
                dht->temperature().getEvent(&dht_event);
                newTemperature = (isnan(dht_event.temperature) ? -127 : dht_event.temperature);
                dht->humidity().getEvent(&dht_event);
                newHumidity = (isnan(dht_event.relative_humidity) ? 0 : dht_event.relative_humidity);
            });
        } break;
        case ThermometerTypes::DS_18B20: {
            onewire = new OneWire(mBusAddress);
            dallastemperature = new DallasTemperature(onewire);
            dallastemperature->begin();

            Updater->OnTimeout([&] {
                dallastemperature->requestTemperatures();
                newTemperature = dallastemperature->getTempCByIndex(0);
                newHumidity = 0;
            });
        } break;
    }
    Updater->Start();
}

void thermometer::Control() {
    Updater->Control();

    if (Scale == TemperatureScales::Fahrenheit) newTemperature = (newTemperature * 9 / 5) + 32;

    if (newTemperature != mTemperature) {
        mTemperature = newTemperature;

        if (mTemperatureChanged != nullptr) mTemperatureChanged();

        if (Device->MQTT != nullptr) {
            Device->MQTT->Publish(String("Get/Thermometer:" + Name() + "/Temperature" + (char)Scale), String(Temperature()));
        }
    }

    if (newHumidity != mHumidity) {
        mHumidity = newHumidity;

        if (mHumidityChanged != nullptr) mHumidityChanged();

        if (Device->MQTT != nullptr) {
            Device->MQTT->Publish(String("Get/Thermometer:" + Name() + "/Humidity"), String(Humidity()));
        }
    }
}