#include "Configuration.h"

#include "Device.h"

configuration::configuration(device *device_obj) : Device(device_obj) {
    bool HasError = false;
    if (LittleFS.begin()) {
        fs::File FileConfiguration = Device->FileSystem->OpenFile(Defaults.Files.Configuration, "r");
        if (FileConfiguration) {
            HasError = (deserializeJson(JsonConfiguration, FileConfiguration.readString()) != DeserializationError::Code::Ok);
        } else
            HasError = true;
    } else
        HasError = true;

    Setting = JsonConfiguration.as<JsonObject>();
    if (!Setting) HasError = true;

    if (HasError) {
        Device->SerialPort->println(Device->TimeStamp() + String("Error while reading " + Defaults.Files.Configuration + " - Resetting to default settings and restarting...").c_str());
        if (DefaultSettings()) {
            Device->Restart();
        } else {
            Device->SerialPort->println(Device->TimeStamp() + String("Error while reading " + Defaults.Files.DefaultSettings + " when resetting to default settings - Unable to continue.").c_str());
            ESP.wdtDisable();
            while (1);
        }
    }
}

int16_t configuration::IndexOf_Component(String name) {
    for (int16_t objs = 0; objs < (int16_t)Device->Configuration->Setting["Components"].size(); objs++)
        if (Device->Configuration->Setting["Components"][objs]["Name"] == name) return objs;
    return -1;
}

bool configuration::Authenticate(String name, String password) {
    int16_t userid = IndexOf_User(name);

    if (userid > -1) {
        if (Device->Configuration->Setting["Security"]["Users"][userid]["Password"].as<String>() == password) return true;
    }

    return false;
}

bool configuration::ChangePassword(String name, String password) {
    int16_t userid = IndexOf_User(name);

    if (userid > -1) {
        Device->Configuration->Setting["Security"]["Users"][userid]["Password"] = password;
        return true;
    }

    return false;
}

int16_t configuration::IndexOf_User(String name) {
    for (uint16_t objs = 0; objs < Device->Configuration->Setting["Security"]["Users"].size(); objs++)
        if (Device->Configuration->Setting["Security"]["Users"][objs]["Name"] == name) return objs;
    return -1;
}

bool configuration::Add_Component(String mdl_class, String name, uint16_t pin, String action) {
    if (AvailableComponentClasses.find(name) == AvailableComponentClasses.end()) {
        JsonObject mdl = Device->Configuration->Setting["Components"].as<JsonArray>().createNestedObject();
        mdl["Class"] = mdl_class;
        mdl["Name"] = name;
        mdl["Pin"] = pin;
        mdl["Action"] = action;
        Device->Configuration->SaveSettings();
        return true;
    }

    return false;
}

bool configuration::Add_User(String name, String password, bool isadmin) {
    if (IndexOf_User(name) == -1) {
        JsonObject mdl = Device->Configuration->Setting["Security"]["Users"].as<JsonArray>().createNestedObject();
        mdl["Name"] = name;
        mdl["Password"] = password;
        mdl["Admin"] = isadmin;
        Device->Configuration->SaveSettings();
        return true;
    }

    return false;
}

bool configuration::Admin_User(String name) {
    int16_t userid = IndexOf_User(name);

    if (userid > -1) {
        return Device->Configuration->Setting["Security"]["Users"][userid]["Admin"].as<bool>();
    }

    return false;
}

bool configuration::Delete_Component(int16_t index) {
    if (index <= Count_Component()) {
        Device->Configuration->Setting["Components"].as<JsonArray>().remove(index);
        Device->Configuration->SaveSettings();
        return true;
    }

    return false;
}

bool configuration::Delete_User(int16_t index) {
    if (index <= Count_User()) {
        Device->Configuration->Setting["Security"]["Users"].as<JsonArray>().remove(index);
        Device->Configuration->SaveSettings();
        return true;
    }

    return false;
}

bool configuration::Delete_Component(String name) {
    uint16_t idx = IndexOf_Component(name);
    if (idx > -1) return Delete_Component(idx);

    return false;
}

bool configuration::Delete_User(String name) {
    uint16_t idx = IndexOf_User(name);
    if (idx > -1) return Delete_User(idx);

    return false;
}

int16_t configuration::Count_Component() {
    return Device->Configuration->Setting["Components"].size();
}

int16_t configuration::Count_User() {
    return Device->Configuration->Setting["Security"]["Users"].size();
}

bool configuration::SaveSettings() {
    fs::File FileConfiguration = Device->FileSystem->OpenFile(Defaults.Files.Configuration, "w");
    if (FileConfiguration) {
        serializeJson(Setting, FileConfiguration);
        FileConfiguration.close();
        return true;
    }
    return false;
}

bool configuration::DefaultSettings() {
    return Device->FileSystem->CopyFile(Defaults.Files.DefaultSettings.c_str(), Defaults.Files.Configuration.c_str());
}