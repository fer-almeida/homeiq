#include "I2C.h"

#include "Device.h"

i2c::i2c(device *device_obj) : Device(device_obj) {
    Wire.begin();
    Scan();
}

void i2c::Enabled(bool value) {
    Device->Configuration->Setting["General"]["I2C Bus"] = value;
    Device->Configuration->SaveSettings();
}

bool i2c::Enabled() {
    return Device->Configuration->Setting["General"]["I2C Bus"].as<bool>();
}

void i2c::Scan() {
    Clear();

    uint8_t data = 0;

    for (uint8_t addr = mStart_Address; addr <= mEnd_Address; addr++) {
        if (twi_writeTo(addr, &data, 0, 1) == 0) {
            mDevices.push_back(addr);
        }
    }
}