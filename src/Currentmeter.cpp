#include "Currentmeter.h"

#include "Device.h"

currentmeter::currentmeter(device *device_obj, uint16_t id, String name, uint8_t busaddress, ComponentBuses bus) : component(name, id, busaddress, bus), Device(device_obj) {
    Event.insert({{"CurrentACChanged", [&](callback_t callback) { mCurrentACChanged = callback; }},
                  {"CurrentDCChanged", [&](callback_t callback) { mCurrentDCChanged = callback; }}});

    mSensor = new ACS712(ACS712_30A, mBusAddress);
    mSensor->calibrate();

    Updater = new timer(Defaults.UpdateRefreshMs);
    Updater->OnTimeout([&] {
        newCurrentAC = mSensor->getCurrentAC();
        newCurrentDC = mSensor->getCurrentDC();
    });
    Updater->Start();
}

void currentmeter::Control() {
    Updater->Control();

    if (newCurrentAC != mCurrentAC) {
        mCurrentAC = newCurrentAC;

        if (mCurrentACChanged != nullptr) mCurrentACChanged();

        if (Device->MQTT != nullptr) {
            Device->MQTT->Publish(String("Get/Currentmeter:" + Name() + "/CurrentAC"), String(CurrentAC()));
        }
    }

    if (newCurrentDC != mCurrentDC) {
        mCurrentDC = newCurrentDC;

        if (mCurrentDCChanged != nullptr) mCurrentDCChanged();

        if (Device->MQTT != nullptr) {
            Device->MQTT->Publish(String("Get/Currentmeter:" + Name() + "/CurrentDC"), String(CurrentAC()));
        }
    }
}
