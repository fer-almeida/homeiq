#include "TelnetServer.h"

#include "Device.h"

telnetserver::telnetserver(device* device_obj) : Device(device_obj) {
    mPort = Device->Network->Telnet_Port();

    onCommand("clear", "Clear terminal screen\r\n\r\nclear", [&](AsyncClient* client, String* parameter) {
        client->write(String("\x1B[2J\x1B[H").c_str());
    });

    onCommand("datetime", "Show current date and time\r\n\r\ndatetime", [&](AsyncClient* client, String* parameter) {
        client->write(String(Device->DateTime->DateTime() + "\r\n\r\n").c_str());
    });

    onCommand("devname", "Show or change the device name\r\n\r\ndevname [new name] (max 30 chars)", [&](AsyncClient* client, String* parameter) {
        String newname;
        for (uint8_t n = 0; n < Defaults.TelnetServer.MaxCommandParameters; n++)
            if (!parameter[n].isEmpty()) newname += (newname.isEmpty() ? "" : " ") + parameter[n];
        newname = SString(newname, 30);

        if (newname.isEmpty()) {
            client->write(String(Device->Name() + "\r\n\r\n").c_str());
        } else {
            if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
                Device->Name(newname);
                client->write(String("New device name: " + newname + "\r\n\r\n").c_str());
            } else {
                client->write(TelnetServer_StringTable.Unauthorized.c_str());
            }
        }
    });

    onCommand("echo", "Echo text\r\n\r\necho text", [&](AsyncClient* client, String* parameter) {
        String Text;
        for (uint8_t n = 0; n < Defaults.TelnetServer.MaxCommandParameters; n++)
            if (!parameter[n].isEmpty()) Text += (Text.isEmpty() ? "" : " ") + parameter[n];

        client->write(String(Text + "\r\n\r\n").c_str());
    });

    onCommand("exit", "Close session and exit terminal\r\n\r\nexit", [&](AsyncClient* client, String* parameter) {
        Device->Log->Write("Telnet client [" + CurrentSession(client)->RemoteIP.toString() + ":" + String(CurrentSession(client)->RemotePort) + "] disconnected", LogTypes::Info);
        mSessions.erase(mSessions.begin() + (SessionID(client) - 1));
        client->write("Session closed.\r\n\r\n");
        client->stop();
    });

    onCommand("free", "Show free RAM\r\n\r\nfree", [&](AsyncClient* client, String* parameter) {
        client->write(String(String(ESP.getFreeHeap()) + " bytes free.\r\n\r\n").c_str());
    });

    onCommand("help", "Show available commands\r\n\r\nhelp", [&](AsyncClient* client, String* parameter) {
        if (parameter[0].isEmpty()) {
            client->write(String("Available commands:\r\n\r\n").c_str());

            String ln;
            uint16_t cmdln = 0;
            for (telnetcommand* cmd : mCommandList) {
                cmdln++;
                String tcmd = SString(cmd->Command).LimitString(Defaults.TelnetServer.CommandSize, true);
                ln += tcmd;
                if (cmdln >= Defaults.TelnetServer.HelpCommandsPerLine) {
                    ln += "\r\n";
                    cmdln = 0;
                }
            }
            ln += "\r\n\r\nUse . to repeat last command.\r\n\r\n";
            client->write(ln.c_str());
        } else {
            bool ValidCommand = false;
            for (telnetcommand* cmd : mCommandList) {
                if (parameter[0] == cmd->Command) {
                    client->write(String(cmd->HelpMessage + "\r\n\r\n").c_str());
                    ValidCommand = true;
                    break;
                }
            }

            if (!ValidCommand) client->write(String(parameter[0] + " - Invalid command.\r\n\r\n").c_str());
        }
    });

    onCommand("httpsvr", "Show or change HTTP Server\r\n\r\nhttpsvr [option]\r\n\r\n[option] list:\r\nenable/disable - Enable or disable HTTP Server\r\nport - View or set HTTP Server port", [&](AsyncClient* client, String* parameter) {
        if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
            if (parameter[0].equalsIgnoreCase("enable")) {
                Device->Network->HTTP_Enabled(true);
            } else if (parameter[0].equalsIgnoreCase("disable")) {
                Device->Network->HTTP_Enabled(false);
            } else if (parameter[0].equalsIgnoreCase("port")) {
                if (!parameter[1].isEmpty()) {
                    uint16_t p = parameter[1].toInt();
                    if (p > 0 && p <= 65535) {
                        Device->Network->HTTP_Port(p);
                    } else {
                        client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                    }
                } else {
                    client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                }
            } else if (parameter[0].isEmpty()) {
                ;
                ;
            }
            client->write(String("HTTP Server: " + String(Device->Network->HTTP_Enabled() ? "Enabled on TCP port " : "Disabled on TCP port ") + String(Device->Network->HTTP_Port()) + "\r\n\r\n").c_str());
        } else {
            client->write(TelnetServer_StringTable.Unauthorized.c_str());
        }
    });

    onCommand("i2c", "Show or change I2C bus status\r\n\r\ni2c [enable/disable]", [&](AsyncClient* client, String* parameter) {
        if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
            if (parameter[0].equalsIgnoreCase("enable")) {
                Device->Configuration->Setting["General"]["I2C Bus"] = true;
                Device->Configuration->SaveSettings();
            } else if (parameter[0].equalsIgnoreCase("disable")) {
                Device->Configuration->Setting["General"]["I2C Bus"] = false;
                Device->Configuration->SaveSettings();
            } else if (parameter[0].isEmpty()) {
                ;
                ;
            } else {
                client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                return;
            }

            client->write(String("I2C bus " + String(Device->Configuration->Setting["General"]["I2C Bus"].as<bool>() ? "enabled" : "disabled") + "\r\n\r\n").c_str());
        } else {
            client->write(TelnetServer_StringTable.Unauthorized.c_str());
        }
    });

    onCommand("log", "Log tool\r\n\r\nlog [option]\r\n\r\n[option] list:\r\nshow - Show local log\r\nclear - Clear local log", [&](AsyncClient* client, String* parameter) {
        if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
            if (parameter[0].isEmpty()) {
                client->write(TelnetServer_StringTable.MissingParameter.c_str());
            } else {
                if (parameter[0].equalsIgnoreCase("show")) {
                    uint16_t LineCount = 0;  //, CurrentCount = 0;

                    fs::File file = Device->FileSystem->OpenFile(Defaults.Files.Log, "r");

                    if (file) {
                        while (file.available())
                            if ((char)file.read() == '\r') LineCount++;
                        file.seek(0);  // Rewind

                        client->write(String("Local Log file (Last " + String((LineCount < 10 ? LineCount : 10)) + " entries):\r\n").c_str());

                        // String Ln;
                        // char c;
                        // while(file.available()) {
                        //     c = (char)file.read();
                        //     Ln += c;

                        //     if (c == '\r') {
                        //         CurrentCount++;

                        //         if (CurrentCount > (LineCount - 10)) { // 10 lines only
                        //             client->write(Ln.c_str());
                        //         }

                        //         Ln.clear();
                        //     }
                        // }

                        file.close();
                    }
                    client->write(String("\r\n\r\n" + String(LineCount) + " log entries total.\r\n\r\n").c_str());
                } else if (parameter[0].equalsIgnoreCase("clear")) {
                    Device->Log->Clear();
                    client->write("Log cleared.\r\n\r\n");
                } else if (parameter[0].equalsIgnoreCase("level")) {
                    if (!parameter[1].isEmpty()) {
                        client->write(String("Log level set to ").c_str());
                    } else {
                        client->write(TelnetServer_StringTable.MissingParameter.c_str());
                    }
                } else if (parameter[0].equalsIgnoreCase("to")) {
                    if (!parameter[1].isEmpty()) {
                        client->write(String("Log to set to ").c_str());
                    } else {
                        client->write(TelnetServer_StringTable.MissingParameter.c_str());
                    }
                } else {
                    client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                }
            }
        } else {
            client->write(TelnetServer_StringTable.Unauthorized.c_str());
        }
    });

    onCommand("logon", "Log into the system with specific credential\r\n\r\nlogon [username] [password]", [&](AsyncClient* client, String* parameter) {
        if (!mParameter[0].isEmpty()) {
            if (!mParameter[1].isEmpty()) {
                if (Device->Configuration->Authenticate(mParameter[0], mParameter[1])) {
                    mSessions[SessionID(client) - 1]->User = mParameter[0];
                    client->write(String("Logon successful for user " + mParameter[0] + ".\r\n\r\n").c_str());
                    Device->Log->Write("Telnet logon successful for '" + mParameter[0] + "@" + client->remoteIP().toString() + ":" + String(client->remotePort()) + "'", LogTypes::Info);
                    return;
                }
                client->write(String("Logon failed for " + mParameter[0] + ".\r\n\r\n").c_str());
                Device->Log->Write("Telnet logon failed for '" + mParameter[0] + "@" + client->remoteIP().toString() + ":" + String(client->remotePort()) + "'", LogTypes::Warning);
            } else {
                client->write(String("Missing password for user " + mParameter[0] + ".\r\n\r\n").c_str());
                Device->Log->Write("Telnet logon failed for '" + mParameter[0] + "@" + client->remoteIP().toString() + ":" + String(client->remotePort()) + "' - Missing password", LogTypes::Warning);
            }
        } else {
            client->write("Missing username and password.\r\n\r\n");
            Device->Log->Write("Telnet logon failed for '" + mParameter[0] + "@" + client->remoteIP().toString() + ":" + String(client->remotePort()) + "' - Missing username and password", LogTypes::Warning);
        }
    });

    onCommand("logout", "Log out the current user\r\n\r\nlogout", [&](AsyncClient* client, String* parameter) {
        Device->Log->Write("Telnet user '" + CurrentSession(client)->User + "@" + client->remoteIP().toString() + ":" + String(client->remotePort()) + "' logged out", LogTypes::Info);
        mSessions[SessionID(client) - 1]->User = Defaults.TelnetServer.GuestUser;
        client->write("User logged out.\r\n\r\n");
    });

    onCommand("mac", "Show device MAC address\r\n\r\nmac", [&](AsyncClient* client, String* parameter) {
        client->write(String(Device->Network->MAC_Address() + "\r\n\r\n").c_str());
    });

    onCommand("cmp", "Component tool\r\n\r\ncomponent [option]\r\n\r\n[option] list:\r\nlist - List components installed\r\ndelete - Delete component\r\nadd - Add component\r\n", [&](AsyncClient* client, String* parameter) {
        if (parameter[0].isEmpty()) {
            client->write(String(TelnetServer_StringTable.MissingParameter.c_str()).c_str());
        } else {
            if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
                if (parameter[0].equalsIgnoreCase("list")) {
                    String stt;

                    client->write(String("Installed components:\r\n\r\nID  | Class           | Name                 | Bus             | Pin | State/Value\r\n").c_str());

                    for (uint16_t objs = 0; objs < Device->Configuration->Setting["Components"].size(); objs++) {
                        if (objs <= (Device->Components.Count() - 1)) {
                            switch (Device->Components[objs]->Class()) {
                                case ComponentClasses::Button: {
                                    stt = String(((button*)Device->Components[objs])->State());
                                } break;
                                case ComponentClasses::Relay: {
                                    stt = String(((relay*)Device->Components[objs])->State() ? "On" : "Off");
                                } break;
                                case ComponentClasses::Thermometer: {
                                    stt = String(String(((thermometer*)Device->Components[objs])->Temperature()) + " C/" + String(((thermometer*)Device->Components[objs])->Humidity()) + " H%");
                                } break;
                                case ComponentClasses::Currentmeter: {
                                    stt = String(String(((currentmeter*)Device->Components[objs])->CurrentAC()) + " AC/" + String(((currentmeter*)Device->Components[objs])->CurrentDC()) + " DC");
                                } break;
                                case ComponentClasses::Blinds: {
                                    stt = String(((blinds*)Device->Components[objs])->Position()) + " (" + (((blinds*)Device->Components[objs])->State() == BlindsState::Closing ? "Closing" : (((blinds*)Device->Components[objs])->State() == BlindsState::Opening ? "Opening" : "Idle")) + ")";
                                } break;
                                default:
                                    break;
                            }
                        } else {
                            stt = "Unknown";
                        }

                        client->write(String(
                            SString(String(objs)).LimitString(3, true) + " | " +
                            SString(Device->Configuration->Setting["Components"][objs]["Class"].as<String>()).LimitString(15, true) + " | " +
                            SString(Device->Configuration->Setting["Components"][objs]["Name"].as<String>()).LimitString(20, true) + " | " +
                            SString(Device->Configuration->Setting["Components"][objs]["Bus"].as<String>()).LimitString(15, true) + " | " +
                            SString(Device->Configuration->Setting["Components"][objs]["Pin"].as<String>()).LimitString(3, true) + " | " +
                            stt +"\r\n").c_str());
                    }

                    client->write(String("\r\n" + String(Device->Configuration->Count_Component()) + " installed component(s), " + String(Device->Components.Count()) + " enabled component(s)\r\n\r\n").c_str());
                } else if (parameter[0].equalsIgnoreCase("refresh")) {
                    Device->Components.Refresh();
                    client->write("All components refreshed successfully.\r\n\r\n");
                } else if (parameter[0].equalsIgnoreCase("add")) {
                    if ((!parameter[1].isEmpty()) && (!parameter[2].isEmpty()) && (!parameter[3].isEmpty()) && (!parameter[4].isEmpty())) {
                        String Action;
                        for (uint8_t n = 4; n < Defaults.TelnetServer.MaxCommandParameters; n++)
                            if (!parameter[n].isEmpty()) Action += (Action.isEmpty() ? "" : " ") + parameter[n];

                        if ((Action.indexOf('[') != -1) && (Action.indexOf(']') != -1))
                            Action = Action.substring(Action.indexOf('[') + 1, Action.indexOf(']'));
                        else
                            Action.clear();

                        if (!Action.isEmpty()) {
                            client->write(String(parameter[1] + " '" + parameter[2] + "' " + (Device->Configuration->Add_Component(parameter[1], parameter[2], parameter[3].toInt(), Action) ? "" : "not ") + "added to configuration.\r\n\r\n").c_str());
                        } else {
                            client->write("Error in Action command.\r\n\r\n");
                        }
                    } else {
                        client->write(TelnetServer_StringTable.MissingParameter.c_str());
                    }
                } else if (parameter[0].equalsIgnoreCase("delete")) {
                    if (!parameter[2].isEmpty()) {
                        if (parameter[1].equalsIgnoreCase("id")) {
                            client->write(String(parameter[1] + " '" + parameter[2] + "' " + (Device->Configuration->Delete_Component(parameter[2].toInt()) ? "" : "not ") + "deleted from configuration.\r\n\r\n").c_str());
                        } else if (parameter[1].equalsIgnoreCase("name")) {
                            client->write(String(parameter[1] + " '" + parameter[2] + "' " + (Device->Configuration->Delete_Component(parameter[2]) ? "" : "not ") + "deleted from configuration.\r\n\r\n").c_str());
                        } else {
                            client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                        }
                    } else {
                        client->write(TelnetServer_StringTable.MissingParameter.c_str());
                    }
                } else {
                    if ((!parameter[0].isEmpty()) && (Device->Components.IndexOf(parameter[0]) > -1)) {
                        if (parameter[1].isEmpty()) {
                            switch (Device->Components[parameter[0]]->Class()) {
                                case ComponentClasses::Button:
                                case ComponentClasses::Relay: {
                                    client->write(String(parameter[0] + " state: " + String(((relay*)Device->Components[parameter[0]])->State()) + "\r\n\r\n").c_str());
                                } break;
                                case ComponentClasses::Thermometer: {
                                    client->write(String(parameter[0] + " temp: " + String(((thermometer*)Device->Components[parameter[0]])->Temperature()) + " C/" + String(((thermometer*)Device->Components[parameter[0]])->Humidity()) + " H%\r\n\r\n").c_str());
                                } break;
                                case ComponentClasses::Currentmeter: {
                                    client->write(String(parameter[0] + " current: " + String(((currentmeter*)Device->Components[parameter[0]])->CurrentAC()) + " AC/" + String(((currentmeter*)Device->Components[parameter[0]])->CurrentDC()) + " DC\r\n\r\n").c_str());
                                } break;
                                case ComponentClasses::Blinds: {
                                    client->write(String(parameter[0] + " position: " + String(((blinds*)Device->Components[parameter[0]])->Position()) + " (" + (((blinds*)Device->Components[parameter[0]])->State() == BlindsState::Closing ? "Closing" : (((blinds*)Device->Components[parameter[0]])->State() == BlindsState::Opening ? "Opening" : "Idle")) + ")\r\n\r\n").c_str());
                                } break;
                                default:
                                    break;
                            }
                        } else {
                            if (parameter[1].equalsIgnoreCase("trigger")) {
                                // if (Device->Components[parameter[0]]->TriggerEvent(parameter[2]) == true) {
                                //     client->write(String("Event '" + parameter[2] + "' triggered successfuly\r\n\r\n").c_str());
                                // } else {
                                //     client->write(String("Invalid trigger event '" + parameter[2] + "'\r\n\r\n").c_str());
                                // }
                            } else {
                                String Result;
                                switch (Device->Components[parameter[0]]->Class()) {
                                    case ComponentClasses::Relay: {
                                        ((relay*)Device->Components[parameter[0]])->State((bool)constrain(parameter[1].toInt(), 0, 1));
                                        Result = String("state to " + String((bool)constrain(parameter[1].toInt(), 0, 1)));
                                    } break;
                                    case ComponentClasses::Blinds: {
                                        ((blinds*)Device->Components[parameter[0]])->Position((uint8_t)constrain(parameter[1].toInt(), 0, 100));
                                        Result = String("position to " + String((uint8_t)constrain(parameter[1].toInt(), 0, 100)));
                                    } break;
                                    default:
                                        break;
                                }
                                client->write(String(parameter[0] + " set " + Result + "\r\n\r\n").c_str());
                            }
                        }
                    } else {
                        client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                    }
                }
            } else {
                client->write(TelnetServer_StringTable.Unauthorized.c_str());
            }
        }
    });

    onCommand("mqtt", "Show or change MQTT settings\r\n\r\nmqtt [option]\r\n\r\n[option] list:\r\nenable/disable - Enable or disable MQTT\r\nbroker - View or set MQTT broker host\r\nport - View or set MQTT broker port\r\nuser - View or set MQTT broker user\r\npassword - View or set MQTT broker password", [&](AsyncClient* client, String* parameter) {
        if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
            if (parameter[0].equalsIgnoreCase("enable")) {
                Device->Network->MQTT_Enabled(true);
            } else if (parameter[0].equalsIgnoreCase("disable")) {
                Device->Network->MQTT_Enabled(false);
            } else if (parameter[0].equalsIgnoreCase("port")) {
                if (!parameter[1].isEmpty()) {
                    uint16_t p = parameter[1].toInt();
                    if (p > 0 && p <= 65535) {
                        Device->MQTT->Broker_Port(p);
                    } else {
                        client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                    }
                } else {
                    client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                }
            } else if (parameter[0].equalsIgnoreCase("broker")) {
                if (!parameter[1].isEmpty()) {
                    Device->MQTT->Broker_URL(parameter[1]);
                } else {
                    client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                }
            } else if (parameter[0].equalsIgnoreCase("user")) {
                if (!parameter[1].isEmpty()) {
                    Device->MQTT->User(parameter[1]);
                } else {
                    client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                }
            } else if (parameter[0].equalsIgnoreCase("password")) {
                if (!parameter[1].isEmpty()) {
                    Device->MQTT->Password(parameter[1]);
                } else {
                    client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                }
            } else if (parameter[0].isEmpty()) {
                ;
                ;
            }
            client->write(String("MQTT " + String(Device->Network->MQTT_Enabled() ? "enabled" : "disabled") + " - " + Device->MQTT->User() + ":" + Device->MQTT->Password() + "@" + Device->MQTT->Broker_URL() + ":" + String(Device->MQTT->Broker_Port()) + "\r\n\r\n").c_str());
        } else {
            client->write(TelnetServer_StringTable.Unauthorized.c_str());
        }
    });

    onCommand("net", "Show or change network configuration\r\n\r\nnet [options]", [&](AsyncClient* client, String* parameter) {
        if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
            if (!parameter[0].isEmpty()) {
                if (parameter[0].equalsIgnoreCase("ip")) {
                    if (!parameter[1].isEmpty()) {
                        if (parameter[1].equalsIgnoreCase("auto")) {
                            Device->Network->DHCP(true);
                        } else {
                            if ((!parameter[1].isEmpty()) && (!parameter[2].isEmpty()) && (!parameter[3].isEmpty())) {
                                Device->Network->DHCP(false);
                                Device->Network->IP_Address(parameter[1]);
                                Device->Network->Netmask(parameter[2]);
                                Device->Network->Gateway(parameter[3]);
                            } else {
                                client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                                return;
                            }
                        }
                        client->write(TelnetServer_StringTable.NewNetworkConfig.c_str());
                    } else {
                        client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                        return;
                    }
                } else if (parameter[0].equalsIgnoreCase("ssid")) {
                    if ((!parameter[1].isEmpty()) && (!parameter[2].isEmpty())) {
                        Device->Network->SSID(parameter[1]);
                        Device->Network->PSK(parameter[2]);
                    } else {
                        client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                        return;
                    }
                    client->write(TelnetServer_StringTable.NewNetworkConfig.c_str());
                } else if (parameter[0].equalsIgnoreCase("hostname")) {
                    if (!parameter[1].isEmpty()) {
                        Device->Network->Hostname(SString(parameter[1], 63));
                    } else {
                        client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                        return;
                    }
                    client->write(TelnetServer_StringTable.NewNetworkConfig.c_str());
                } else if (parameter[0].equalsIgnoreCase("dns")) {
                    if (!parameter[1].isEmpty()) {
                        Device->Network->DNS_Server(0, parameter[1]);
                        Device->Network->DNS_Server(1, (parameter[2].isEmpty() ? "" : parameter[2]));
                    } else {
                        client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                        return;
                    }
                    client->write(TelnetServer_StringTable.NewNetworkConfig.c_str());
                } else if (parameter[0].equalsIgnoreCase("ntp")) {
                    if (!parameter[1].isEmpty()) {
                        Device->Network->NTP_Server(parameter[1]);
                    } else {
                        client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                        return;
                    }
                    client->write(TelnetServer_StringTable.NewNetworkConfig.c_str());
                } else {
                    client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                    return;
                }
            } else {
                client->write(String("SSID: " + Device->Network->SSID() + " [" + Device->Network->PSK() + "]\r\n" +
                                     "Hostname: " + Device->Network->Hostname() + "\r\n" +
                                     "NTP Server: " + Device->Network->NTP_Server() + "\r\n" +
                                     "DHCP Client: " + String(Device->Network->DHCP() ? "Enabled" : "Disabled") + "\r\n" +
                                     "IP: " + Device->Network->IP_Address().toString() + "\r\n" +
                                     "Netmask: " + Device->Network->Netmask().toString() + "\r\n" +
                                     "Gateway: " + Device->Network->Gateway().toString() + "\r\n" +
                                     "DNS Servers: [1] " + Device->Network->DNS_Server(0).toString() + " [2] " + Device->Network->DNS_Server(1).toString() + "\r\n\r\n")
                                  .c_str());
            }
        } else {
            client->write(TelnetServer_StringTable.Unauthorized.c_str());
        }
    });

    onCommand("reset", "Reset all configuration to default settings\r\n\r\nreset", [&](AsyncClient* client, String* parameter) {
        if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
            if (Device->Configuration->DefaultSettings()) {
                client->write("All configuration reset back to default settings.\r\n\r\n");
            } else {
                client->write("Failed to reset configuration to default settings.\r\n\r\n");
            }
        } else {
            client->write(TelnetServer_StringTable.Unauthorized.c_str());
        }
    });

    onCommand("restart", "Restart the device or change the auto restart interval\r\n\r\nrestart [option]\r\n\r\n[option] list:\r\nnow - Restart now\r\n(min " + String(Defaults.RestartTimer.MinInterval) + ", max " + String(Defaults.RestartTimer.MaxInterval) + ") - Seconds set", [&](AsyncClient* client, String* parameter) {
        if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
            if (!parameter[0].isEmpty()) {
                if (parameter[0].equalsIgnoreCase("now")) {
                    Device->Restart();
                } else if (parameter[0].equalsIgnoreCase("when")) {
                    if (parameter[1].equalsIgnoreCase("softap")) {
                        Device->RestartWhen(APMode::SoftAP);
                    } else if (parameter[1].equalsIgnoreCase("offline")) {
                        Device->RestartWhen(APMode::Offline);
                    } else if (parameter[1].equalsIgnoreCase("wificlient")) {
                        Device->RestartWhen(APMode::WifiClient);
                    } else {
                        Device->RestartWhen(APMode::None);
                    }

                    String RestartWhenStr;

                    switch (Device->RestartWhen()) {
                        case APMode::SoftAP: {
                            RestartWhenStr = "SoftAP";
                        } break;
                        case APMode::Offline: {
                            RestartWhenStr = "Offline/No connection";
                        } break;
                        case APMode::WifiClient: {
                            RestartWhenStr = "WiFi client";
                        } break;
                        default: {
                            RestartWhenStr = "Any mode";
                        } break;
                    }

                    client->write(String("Auto restart set when: " + RestartWhenStr + "\r\n\r\n").c_str());
                } else {
                    Device->RestartInterval((uint16_t)parameter[0].toInt());
                }
            }
            client->write(String("Auto restart " + (Device->RestartInterval() == 0 ? "disabled." : "interval set to " + String(Device->RestartInterval()) + " seconds.") + "\r\n\r\n").c_str());
        } else {
            client->write(TelnetServer_StringTable.Unauthorized.c_str());
        }
    });

    onCommand("rssi", "Show device WIFI RSSI\r\n\r\nrssi", [&](AsyncClient* client, String* parameter) {
        client->write(String("RSSI: " + String(WiFi.RSSI()) + " dBm\r\n\r\n").c_str());
    });

    onCommand("serial", "Show serial number\r\n\r\nserial", [&](AsyncClient* client, String* parameter) {
        if (!parameter[0].isEmpty()) {
            if (Device->GetSerialNumber().equals("---NOT SET---")) {
                String newserialnumber = parameter[0];
                newserialnumber.toUpperCase();

                Device->SetSerialNumber(newserialnumber);
            }
        }

        client->write(String(Device->GetSerialNumber() + "\r\n\r\n").c_str());
    });

    onCommand("sessions", "Show current Telnet sessions\r\n\r\nsessions", [&](AsyncClient* client, String* parameter) {
        client->write("Current sessions:\r\n\r\n");
        for (telnetsession* session : mSessions) {
            client->write(String(session->User + "@" + session->RemoteIP.toString() + ":" + String(session->RemotePort) + "\r\n").c_str());
        }
        client->write(String("\r\n").c_str());
    });

    onCommand("telnetsvr", "Show or change Telnet Server\r\n\r\ntelnetsvr [option]\r\n\r\n[option] list:\r\nenable/disable - Enable or disable Telnet Server\r\nport - View or set Telnet Server port", [&](AsyncClient* client, String* parameter) {
        if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
            if (parameter[0].equalsIgnoreCase("enable")) {
                Enabled(true);
            } else if (parameter[0].equalsIgnoreCase("disable")) {
                Enabled(false);
            } else if (parameter[0].equalsIgnoreCase("port")) {
                if (!parameter[1].isEmpty()) {
                    uint16_t p = parameter[1].toInt();
                    if (p > 0 && p <= 65535) {
                        Port(p);
                    } else {
                        client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                    }
                } else {
                    client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                }
            } else if (parameter[0].isEmpty()) {
                ;
                ;
            }
            client->write(String("Telnet Server: " + String(Device->Network->Telnet_Enabled() ? "Enabled on TCP port " : "Disabled on TCP port ") + String(Device->Network->Telnet_Port()) + "\r\n\r\n").c_str());
        } else {
            client->write(TelnetServer_StringTable.Unauthorized.c_str());
        }
    });

    onCommand("tz", "Show or change timezone\r\n\r\ntz [timezone]", [&](AsyncClient* client, String* parameter) {
        if (!parameter[0].isEmpty()) {
            if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
                Device->Network->NTP_Timezone(parameter[0].toInt());
            } else {
                client->write(TelnetServer_StringTable.Unauthorized.c_str());
            }
        }

        client->write(String("GMT " + String(Device->DateTime->Timezone() > 0 ? "+" : "") + String(Device->DateTime->Timezone()) + "\r\n\r\n").c_str());
    });

    onCommand("uptime", "Show device uptime running\r\n\r\nuptime", [&](AsyncClient* client, String* parameter) {
        client->write(String(Device->UpTime() + "\r\n\r\n").c_str());
    });

    onCommand("user", "User management\r\n\r\nuser [option]\r\n\r\n[option] list:\r\nlist - List users\r\ndelete - Delete user\r\nadd - Add user\r\nchpass - Change password", [&](AsyncClient* client, String* parameter) {
        if (parameter[0].isEmpty()) {
            client->write(String(TelnetServer_StringTable.MissingParameter.c_str()).c_str());
        } else {
            if (parameter[0].equalsIgnoreCase("chpass")) {
                if (CurrentSession(client)->User == Defaults.TelnetServer.GuestUser) {
                    client->write("Unauthorized - Requires to be logged.\r\n\r\n");
                } else {
                    if ((parameter[1] == CurrentSession(client)->User) || (Device->Configuration->Admin_User(CurrentSession(client)->User))) {
                        if ((!parameter[1].isEmpty()) && (!parameter[2].isEmpty())) {
                            if (Device->Configuration->ChangePassword(parameter[1], parameter[2])) {
                                client->write(String("Password changed for user " + parameter[1] + ".\r\n\r\n").c_str());
                            } else {
                                client->write(String("Error changing password for user " + parameter[1] + ".\r\n\r\n").c_str());
                            }
                        } else {
                            client->write(TelnetServer_StringTable.MissingParameter.c_str());
                        }
                    } else {
                        client->write(String("Error changing password for user " + parameter[1] + ".\r\n\r\n").c_str());
                    }
                }
            } else {
                if (Device->Configuration->Admin_User(CurrentSession(client)->User)) {
                    if (parameter[0].equalsIgnoreCase("list")) {
                        client->write(String("User list:\r\n\r\nID   | Name            | Admin\r\n").c_str());

                        for (uint16_t objs = 0; objs < Device->Configuration->Setting["Security"]["Users"].size(); objs++) {
                            client->write(String(SString(objs).LimitString(4, true) + " | " + SString(Device->Configuration->Setting["Security"]["Users"][objs]["Name"].as<String>()).LimitString(15, true) + " | " + SString(Device->Configuration->Setting["Security"]["Users"][objs]["Admin"].as<String>()).LimitString(5, true) + "\r\n").c_str());
                        }

                        client->write(String("\r\n" + String(Device->Configuration->Count_User()) + " user(s)\r\n\r\n").c_str());
                    } else if (parameter[0].equalsIgnoreCase("add")) {
                        if ((!parameter[1].isEmpty()) && (!parameter[2].isEmpty())) {
                            bool newadminuser = false;
                            if (parameter[3].equalsIgnoreCase("admin")) newadminuser = true;

                            if (Device->Configuration->Add_User(parameter[1], parameter[2], newadminuser)) {
                                client->write(String("User " + parameter[1] + " added" + (newadminuser ? " with admin privileges" : "") + ".\r\n\r\n").c_str());
                            } else {
                                client->write("Error adding user.\r\n\r\n");
                            }
                        } else {
                            client->write(TelnetServer_StringTable.MissingParameter.c_str());
                        }
                    } else if (parameter[0].equalsIgnoreCase("delete")) {
                        if (!parameter[1].isEmpty()) {
                            if (parameter[1] != CurrentSession(client)->User) {
                                int16_t userid = Device->Configuration->IndexOf_User(parameter[1]);

                                if (userid > -1) {
                                    if (Device->Configuration->Delete_User(userid)) {
                                        client->write("User deleted.\r\n\r\n");
                                    } else {
                                        client->write("Error deleting user.\r\n\r\n");
                                    }
                                } else {
                                    client->write("User not found.\r\n\r\n");
                                }
                            } else {
                                client->write("Unable to delete current user.\r\n\r\n");
                            }
                        } else {
                            client->write(TelnetServer_StringTable.MissingParameter.c_str());
                        }
                    } else {
                        client->write(TelnetServer_StringTable.InvalidParameter.c_str());
                    }
                } else {
                    client->write(TelnetServer_StringTable.Unauthorized.c_str());
                }
            }
        }
    });

    onCommand("ver", "Show device firmware version\r\n\r\nver", [&](AsyncClient* client, String* parameter) {
        client->write(String(Version.ProductFamily + " " + Version.ProductName + " " + Version.Software.Info() + " on HW " + Version.Hardware.Info() + "\r\n\r\n").c_str());
    });

    onCommand("whoami", "Show information about current user\r\n\r\nwhoami", [&](AsyncClient* client, String* parameter) {
        client->write(String(CurrentSession(client)->User + "@" + client->remoteIP().toString() + ":" + String(client->remotePort()) + (Device->Configuration->Admin_User(CurrentSession(client)->User) ? " *" : "") + "\r\n\r\n").c_str());
    });

    mServer = new AsyncServer(mPort);

    mServer->onClient([&](void* s, AsyncClient* client) {
        mSessions.push_back(new telnetsession({client->remoteIP(), client->remotePort(), Defaults.TelnetServer.GuestUser}));

        client->write(String(":: " + Device->Name() + " [" + Device->Network->Hostname() + "] CLI - Welcome\r\n\r\n" + Prompt).c_str());
        Device->Log->Write("Telnet client [" + client->remoteIP().toString() + ":" + String(client->remotePort()) + "] connected", LogTypes::Info);

        client->onData([&](void* arg, AsyncClient* client, void* data, size_t len) {
            if (client->space() > 32 && client->canSend()) {
                mIncomeData = String((char*)data).substring(0, len);
                mIncomeData.trim();

                if (mIncomeData.equals("."))
                    mIncomeData = mLastIncomeData; // Restore last command and parammeters
                else
                    if (mIncomeData.length() > 0) mLastIncomeData = mIncomeData; // Save last command and parammeters

                mCommand = mIncomeData.substring(0, mIncomeData.indexOf(' '));
                mIncomeData = mIncomeData.substring(mCommand.length() + 1, mIncomeData.length());

                for (uint8_t i = 0; i < Defaults.TelnetServer.MaxCommandParameters; i++) {
                    if (!mIncomeData.isEmpty()) {
                        mParameter[i] = mIncomeData.substring(0, mIncomeData.indexOf(' '));
                        mIncomeData = mIncomeData.substring(mParameter[i].length() + 1, mIncomeData.length());
                    } else {
                        mParameter[i].clear();
                    }
                }

                mIncomeData.clear();

                if (!mCommand.isEmpty()) {
                    bool ValidCommand = false;

                    for (telnetcommand* cmd : mCommandList) {
                        if (mCommand == cmd->Command) {
                            if ((mParameter[0].equalsIgnoreCase("-h")) || (mParameter[0].equalsIgnoreCase("-?")) || (mParameter[0].equalsIgnoreCase("--help"))) {
                                client->write(String(cmd->HelpMessage + "\r\n\r\n").c_str());
                            } else {
                                mOnCommandCallback = cmd->Callback;
                                mOnCommandCallback(client, mParameter);
                            }

                            ValidCommand = true;
                            break;
                        }
                    }

                    if (!ValidCommand) client->write(String(mCommand + " - Invalid command.\r\n\r\n").c_str());
                }

                client->write(Prompt.c_str());
            }
        },
                       NULL);

        client->onDisconnect([&](void* arg, AsyncClient* client) {
        },
                             NULL);

        client->onError([&](void* arg, AsyncClient* client, int8_t error) {
        },
                        NULL);

        client->onTimeout([&](void* arg, AsyncClient* client, uint32_t time) {
        },
                          NULL);
    },
                      NULL);

    mServer->begin();
}

uint16_t telnetserver::Port(uint16_t value) {
    if (value > 0) mPort = value;
    Device->Network->Telnet_Port(mPort);
    return mPort;
}

void telnetserver::Enabled(bool value) {
    mEnabled = value;
    Device->Network->Telnet_Enabled(mEnabled);
}

uint8_t telnetserver::SessionID(AsyncClient* client) {
    uint8_t ret = 0;
    for (telnetsession* session : mSessions) {
        ret++;
        if ((session->RemoteIP == client->remoteIP()) && (session->RemotePort == client->remotePort())) return ret;
    }
    return 0;
}