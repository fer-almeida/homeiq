#include "Blinds.h"

blinds::blinds(device *device_obj, uint16_t id, String name, relay* relayup, relay* relaydown) : component(name, id), Device(device_obj), mRelayUp(relayup), mRelayDown(relaydown) {
    Event.insert({{"Closed", [&](callback_t callback) { mClosedCallback = callback; }},
                  {"Opened", [&](callback_t callback) { mOpenedCallback = callback; }},
                  {"BeforeClose", [&](callback_t callback) { mBeforeCloseCallback = callback; }},
                  {"BeforeOpen", [&](callback_t callback) { mBeforeOpenCallback = callback; }}});

    mBus = ComponentBuses::Group;
    mBusAddress = 0;
    
    mTimerUp = new timer(mStepMs);
    mTimerDown = new timer(mStepMs);

    mTimerUp->OnTimeout([&] {
        if (mCurrentPosition == mTargetPosition) {
            mTimerUp->Stop();

            mRelayUp->State(false);
            mRelayDown->State(false);

            mState = BlindsState::Idle;
            if (Device->MQTT != nullptr) Device->MQTT->Publish(String("Get/Blinds:" + Name() + "/State"), String(StateToString(State())));
        } else {
            if (mCurrentPosition < mTargetPosition) {
                mCurrentPosition++;

                mRelayUp->State(true, false);
                mRelayDown->State(false, false);
            }

            if ((mCurrentPosition == 100) && (mOpenedCallback != nullptr)) mOpenedCallback();
            if (Device->MQTT != nullptr) Device->MQTT->Publish(String("Get/Blinds:" + mName), String(Position()));
        }

        Device->Configuration->Setting["Components"][mID]["Position"] = Position();
        Device->Configuration->SaveSettings();
    });

    mTimerDown->OnTimeout([&] {
        if (mCurrentPosition == mTargetPosition) {
            mTimerDown->Stop();

            mRelayUp->State(false);
            mRelayDown->State(false);

            mState = BlindsState::Idle;
            if (Device->MQTT != nullptr) Device->MQTT->Publish(String("Get/Blinds:" + mName + "/State"), String(StateToString(State())));
        } else {
            if (mCurrentPosition > mTargetPosition) {
                mCurrentPosition--;

                mRelayUp->State(false, false);
                mRelayDown->State(true, false);
            }

            if ((mCurrentPosition == 0) && (mClosedCallback != nullptr)) mClosedCallback();
            if (Device->MQTT != nullptr) Device->MQTT->Publish(String("Get/Blinds:" + mName), String(Position()));
        }

        Device->Configuration->Setting["Components"][ID()]["Position"] = Position();
        Device->Configuration->SaveSettings();
    });
}

void blinds::Position(uint8_t value, bool setformerposition) {
    value = constrain(value, 0, 100);
    
    if (setformerposition) {  // Blinds does not change physically, just update the former position
        mCurrentPosition = value;
        mTargetPosition = value;
    } else {
        if (mState != BlindsState::Idle) {
            mRelayUp->State(false, false);
            mRelayDown->State(false, false);

            mTimerUp->Stop();
            mTimerDown->Stop();
        }

        if (value > mCurrentPosition) {  // Opening
            if (mBeforeOpenCallback != nullptr) mBeforeOpenCallback();

            if (mCancel) {
                mCancel = false;
            } else {
                mRelayDown->State(false, false);
                mTargetPosition = value;

                mTimerUp->Start();
                mState = BlindsState::Opening;
            }
        } else if (value < mCurrentPosition) {  // Closing
            if (mBeforeCloseCallback != nullptr) mBeforeCloseCallback();

            if (mCancel) {
                mCancel = false;
            } else {
                mRelayUp->State(false, false);
                mTargetPosition = value;

                mTimerDown->Start();
                mState = BlindsState::Closing;
            }
        }
    }
    
    if (Device->MQTT != nullptr) Device->MQTT->Publish(String("Get/Blinds:" + Name() + "/State"), String(StateToString(State())));
}

void blinds::Refresh() {
    if (Device->MQTT != nullptr) {
        Device->MQTT->Publish(String("Get/Blinds:" + Name()), String(Position()));
        Device->MQTT->Publish(String("Get/Blinds:" + Name() + "/State"), String(StateToString(State())));
    }
}

void blinds::Control() {
    mTimerUp->Control();
    mTimerDown->Control();
}