#include "Button.h"

#include "Device.h"

button::button(device *device_obj, uint16_t id, String name, uint8_t busaddress, ComponentBuses bus) : component(name, id, busaddress, bus), Device(device_obj) {
    Event.insert({{"Pressed", [&](callback_t callback) { mPressedCallback = callback; }},
                  {"Released", [&](callback_t callback) { mReleasedCallback = callback; }},
                  {"Changed", [&](callback_t callback) { mChangedCallback = callback; }},
                  {"Clicked", [&](callback_t callback) { mClickedCallback = callback; }},
                  {"LongClicked", [&](callback_t callback) { mLongClickedCallback = callback; }},
                  {"DoubleClicked", [&](callback_t callback) { mDoubleClickedCallback = callback; }},
                  {"TripleClicked", [&](callback_t callback) { mTripleClickedCallback = callback; }}});

    mLongClick_Detected_Retriggerable = false;
    mPressedState = LOW; // (activeLow ? LOW : HIGH);
    DebounceTime(Defaults.Components.Button.DebounceMs);
    LongClickTime(Defaults.Components.Button.LongClickMs);
    DoubleClickTime(Defaults.Components.Button.DoubleClickMs);

    switch (mBus) {
        case ComponentBuses::I2C: {
            pcf8574 = new PCF8574(mBusAddress);
            pcf8574->begin();
        } break;
        default: {
            pinMode(mBusAddress, INPUT_PULLUP);
        } break;
    }

    mState = digitalRead(mBusAddress);
    mPrev_State = mState;
}

void button::Control() {
    mPrev_State = mState;
    uint32_t now = millis();

    switch (mBus) {
        case ComponentBuses::I2C: {
            mState = pcf8574->readButton(mBusAddress);
        } break;
        default: {
            mState = digitalRead(mBusAddress);
        } break;
    }

    if (mState == mPressedState && mPrev_State != mPressedState) {
        mDown_Ms = now;
        mPressed_Triggered = false;
        mClick_Ms = mDown_Ms;
    } else if (mState == mPressedState && !mPressed_Triggered && (now - mDown_Ms >= mDebounce_Time_Ms)) {
        mPressed_Triggered = true;
        mClick_Count++;

        if (mChangedCallback) mChangedCallback();
        if (mPressedCallback) mPressedCallback();
    } else if (mState != mPressedState && mPrev_State == mPressedState) {
        mDown_Time_Ms = now - mDown_Ms;

        if (mDown_Time_Ms >= mDebounce_Time_Ms) {
            if (mChangedCallback) mChangedCallback();
            if (mReleasedCallback) mReleasedCallback();
            if (mDown_Time_Ms >= mLongClick_Time_Ms) mLongClick_Detected = true;
        }
    } else if (mState != mPressedState && now - mClick_Ms > mDoubleClick_Time_Ms) {
        if (mLongClick_Detected) {
            if (mClick_Count == 1) {
                mLast_Click_Type = ClickTypes::Long;
                if (mLongClickedCallback) mLongClickedCallback();
            }

            mLongClick_Detected = false;
            mLongClick_Detected_Reported = false;
            mLongClick_Detected_Counter = 0;
        } else if (mClick_Count > 0) {
            switch (mClick_Count) {
                case 1: {
                    mLast_Click_Type = ClickTypes::Single;
                    if (mClickedCallback) mClickedCallback();
                } break;
                case 2: {
                    mLast_Click_Type = ClickTypes::Double;
                    if (mDoubleClickedCallback) mDoubleClickedCallback();
                } break;
                case 3: {
                    mLast_Click_Type = ClickTypes::Triple;
                    if (mTripleClickedCallback) mTripleClickedCallback();
                } break;
            }
            
            if (Device->MQTT != nullptr) Device->MQTT->Publish(String("Get/Button:" + mName), String(mLast_Click_Type));
        }

        mClick_Count = 0;
        mClick_Ms = 0;
    }

    bool longclick_period_detected = now - mDown_Ms >= (mLongClick_Time_Ms * (mLongClick_Detected_Counter + 1));
    if (mState == mPressedState && longclick_period_detected && !mLongClick_Detected_Reported) {
        mLongClick_Detected_Reported = true;
        mLongClick_Detected = true;
        if (mLongClick_Detected_Retriggerable) {
            mLongClick_Detected_Counter++;
            mLongClick_Detected_Reported = false;
        }

        if (mLongClickedCallback) mLongClickedCallback();
    }
}

void button::Refresh() {
    if (Device->MQTT != nullptr) Device->MQTT->Publish(String("Get/Button:" + mName), String(mLast_Click_Type));
}