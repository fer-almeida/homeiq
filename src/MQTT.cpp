#include "MQTT.h"

#include "Device.h"

WiFiClient wifiClient;

mqtt::mqtt(device* device_obj) : Device(device_obj) {
    Connect();
}

void mqtt::Broker_URL(String value) {
    value.toLowerCase();
    Device->Configuration->Setting["Network"]["MQTT Broker URL"] = value;
    Device->Configuration->SaveSettings();
}

String mqtt::Broker_URL() {
    return Device->Configuration->Setting["Network"]["MQTT Broker URL"].as<String>();
}

void mqtt::Broker_Port(uint16_t value) {
    Device->Configuration->Setting["Network"]["MQTT Broker Port"] = value;
    Device->Configuration->SaveSettings();
}

uint16_t mqtt::Broker_Port() {
    return Device->Configuration->Setting["Network"]["MQTT Broker Port"].as<uint16_t>();
}

void mqtt::User(String value) {
    Device->Configuration->Setting["Network"]["MQTT User"] = value;
    Device->Configuration->SaveSettings();
}

String mqtt::User() {
    return Device->Configuration->Setting["Network"]["MQTT User"].as<String>();
}

void mqtt::Password(String value) {
    Device->Configuration->Setting["Network"]["MQTT Password"] = value;
    Device->Configuration->SaveSettings();
}

String mqtt::Password() {
    return Device->Configuration->Setting["Network"]["MQTT Password"].as<String>();
}

void mqtt::Connect() {
    if (Device->Network->MQTT_Enabled() && Device->Network->Status() == ConnectionStatus::Connected) {
        char tmpMQTT_Name[32], tmpMQTT_User[32], tmpMQTT_Password[32], tmpMQTT_Broker[128], tmpMQTT_Topic[128];

        Device->Network->Hostname().toCharArray(tmpMQTT_Name, 32);
        User().toCharArray(tmpMQTT_User, 32);
        Password().toCharArray(tmpMQTT_Password, 32);
        Broker_URL().toCharArray(tmpMQTT_Broker, 128);
        String(Device->Network->Hostname() + "/Set/#").toCharArray(tmpMQTT_Topic, 128);

        if (mMQTT != nullptr) {  // Falls here also when MQTT credentials are incorrect
            Device->Log->Write("MQTT: Reconnecting - Check client credentials or broker URL/port", LogTypes::Error);
            mMQTT = nullptr;
        }

        mMQTT = new PubSubClient(tmpMQTT_Broker, Broker_Port(), wifiClient);
        if (!mMQTT->connected()) {
            if (mMQTT->connect(tmpMQTT_Name, tmpMQTT_User, tmpMQTT_Password)) {
                mMQTT->subscribe(tmpMQTT_Topic);
                mMQTT->setCallback([&](char* topic, byte* payload, size_t length) {
                    String mTopic(topic);
                    //String mData = CharArrayPointerToString((char*)payload, length);

                    String mData = SString(payload, length);
                    String mComponentType = mTopic.substring(Device->Network->Hostname().length() + 5, mTopic.indexOf(':', Device->Network->Hostname().length() + 1));
                    String mComponentName = mTopic.substring(Device->Network->Hostname().length() + mComponentType.length() + 6, mTopic.indexOf('/', Device->Network->Hostname().length() + mComponentType.length() + 1));

                    Device->Log->Write("MQTT message arrived: [" + mTopic + "] " + mData, LogTypes::Info);

                    if (Device->Components.IndexOf(mComponentName) > -1) {
                        if (mComponentType.equalsIgnoreCase("relay")) {
                            ((mData.equalsIgnoreCase("1") || mData.equalsIgnoreCase("true")) ? ((relay*)Device->Components[mComponentName])->State(true) : ((relay*)Device->Components[mComponentName])->State(false));
                        }
                        if (mComponentType.equalsIgnoreCase("blinds")) {
                            ((blinds*)Device->Components[mComponentName])->Position(mData.toInt());
                        }
                    } else {
                        Device->Log->Write("Component [" + mComponentType + "]:[" + mComponentName + "] not found", LogTypes::Error);
                    }
                });
            }
        }
    }
}

void mqtt::Publish(String message) {
    Publish("", message);
}

void mqtt::Publish(String subtopic, String message) {
    if (Device->Network->MQTT_Enabled() && Device->Network->Status() == ConnectionStatus::Connected) {
        if (!mMQTT->connected()) Connect();
        if (!subtopic.isEmpty()) subtopic = "/" + subtopic;
        mMQTT->publish(String(Device->Network->Hostname() + subtopic).c_str(), message.c_str());
    }
}

void mqtt::Control() {
    if (Device->Network->MQTT_Enabled()) {
        mMQTT->loop();
        if (!mMQTT->connected()) Connect();
    }
}