#include "LogTool.h"

#include "Device.h"

logtool::logtool(device *device_obj) : Device(device_obj) {
    mLogTo = Device->Configuration->Setting["Log"]["Log To"].as<uint8_t>();
    mLogLevel = Device->Configuration->Setting["Log"]["Log Level"].as<uint8_t>();
}

void logtool::WriteToSerialPort(String Message) {
    Device->SerialPort->println(Device->TimeStamp() + Message);
}

void logtool::WriteToSyslogServer(String Message) {
    if (Device->Network != nullptr) {
        if (Device->Network->Mode() == APMode::WifiClient) {
            if (UDPClient == nullptr) {
                UDPClient = new WiFiUDP();
                UDPClient->begin(2390);
            }

            UDPClient->beginPacket(Device->Configuration->Setting["Log"]["Syslog Server"].as<String>().c_str(), Device->Configuration->Setting["Log"]["Syslog Port"].as<uint16_t>());
            UDPClient->write(String(Device->Configuration->Setting["Network"]["Hostname"].as<String>() + " " + Version.ProductName + ": " + Message).c_str());
            UDPClient->endPacket();
        }
    }
}

void logtool::WriteToFile(String Message) {
    if (Device->FileSystem != nullptr) {
        if (Device->FileSystem->PercentFree() >= 5) {
            fs::File file = Device->FileSystem->OpenFile(Defaults.Files.Log, "a");
            if (file) {
                file.println(Device->TimeStamp() + Message);
                file.close();
            }
        }
    }
}

void logtool::Write(String Message, LogTypes LogType) {
    char LogType_C = 'U';  // Undefined
    switch (LogType) {
        case LogTypes::Error:
            if (CHECK_BIT(mLogLevel, 1) || CHECK_BIT(mLogLevel, 4)) LogType_C = 'E';
            break;
        case LogTypes::Warning:
            if (CHECK_BIT(mLogLevel, 2) || CHECK_BIT(mLogLevel, 4)) LogType_C = 'W';
            break;
        case LogTypes::Info:
            if (CHECK_BIT(mLogLevel, 3) || CHECK_BIT(mLogLevel, 4)) LogType_C = 'I';
            break;
        case LogTypes::Debug:
            if (CHECK_BIT(mLogLevel, 4)) LogType_C = 'D';
            break;
    }

    if (LogType_C != 'U') {
        if (CHECK_BIT(mLogTo, 1)) WriteToSerialPort(String(LogType_C) + " " + Message);
        if (CHECK_BIT(mLogTo, 2)) WriteToSyslogServer(String(LogType_C) + " " + Message);
        if (CHECK_BIT(mLogTo, 3)) WriteToFile(String(LogType_C) + " " + Message);
    }
}

void logtool::Clear() {
    fs::File file = Device->FileSystem->OpenFile(Defaults.Files.Log, "w");
    if (file) {
        file.print("");
        file.close();
        Write("Log Cleared", LogTypes::Info);
    } else {
        Write("Log clear error", LogTypes::Error);
    }
}