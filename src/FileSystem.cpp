#include "FileSystem.h"

filesystem::filesystem() {
    LittleFS.begin();
    LittleFS.info(info);
}

bool filesystem::CopyFile(const String &origin, const String &destination) {
    fs::File o = OpenFile(origin, "r");
    fs::File d = OpenFile(destination, "w");

    if (o && d) {
        while (o.available()) d.write(o.read());
        o.close();
        d.close();
        return true;
    } else
        return false;
}